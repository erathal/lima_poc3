package ch.ethz.matsim.lima_poc3.dmc;

import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaModeChoiceConfigGroup extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "lima_mode_choice";

	public static final String USE_DISCRETE_MODE_CHOICE = "useDiscreteModeChoice";
	public static final String USE_IMPORTANCE_SAMPLING = "useImportanceSampling";
	public static final String OPERATING_AREA_LINK_ATTRIBUTE = "operatingAreaLinkAttribute";

	private boolean useDiscreteModeChoice = true;
	private boolean useImportanceSampling = true;
	private String operatingAreaLinkAttribute = "avOperatingArea";

	public LimaModeChoiceConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_DISCRETE_MODE_CHOICE)
	public boolean getUseDiscreteModeChoice() {
		return useDiscreteModeChoice;
	}

	@StringSetter(USE_DISCRETE_MODE_CHOICE)
	public void setUseDiscreteModeChoice(boolean useDiscreteModeChoice) {
		this.useDiscreteModeChoice = useDiscreteModeChoice;
	}

	@StringGetter(USE_IMPORTANCE_SAMPLING)
	public boolean getUseImportanceSampling() {
		return useImportanceSampling;
	}

	@StringSetter(USE_IMPORTANCE_SAMPLING)
	public void setUseImportanceSampling(boolean useImportanceSampling) {
		this.useImportanceSampling = useImportanceSampling;
	}

	@StringGetter(OPERATING_AREA_LINK_ATTRIBUTE)
	public String getOperatingAreaLinkAttribute() {
		return operatingAreaLinkAttribute;
	}

	@StringSetter(OPERATING_AREA_LINK_ATTRIBUTE)
	public void setOperatingAreaLinkAttribute(String operatingAreaLinkAttribute) {
		this.operatingAreaLinkAttribute = operatingAreaLinkAttribute;
	}
}
