package ch.ethz.matsim.lima_poc3.dmc;

import org.matsim.core.router.TripRouter;
import org.matsim.core.scoring.functions.ScoringParametersForPerson;
import org.matsim.facilities.ActivityFacilities;

import com.google.inject.Provides;

import ch.ethz.matsim.discrete_mode_choice.components.utils.PTWaitingTimeEstimator;
import ch.ethz.matsim.discrete_mode_choice.modules.AbstractDiscreteModeChoiceExtension;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.MATSimTripScoringConfigGroup;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameterForPerson;

public class LimaDiscreteModeChoiceModule extends AbstractDiscreteModeChoiceExtension {
	public static final String ESTIMATOR_NAME = "lima";

	@Override
	protected void installExtension() {
		bindTripEstimator(ESTIMATOR_NAME).to(LimaTripScoringEstimator.class);
	}

	@Provides
	public LimaTripScoringEstimator provideLimaTripScoringEstimator(ActivityFacilities facilities,
			TripRouter tripRouter, PTWaitingTimeEstimator waitingTimeEstimator,
			ScoringParametersForPerson scoringParametersForPerson,
			LIMAScoringParameterForPerson limaScoringParametersForPerson, DiscreteModeChoiceConfigGroup dmcConfig) {
		MATSimTripScoringConfigGroup config = dmcConfig.getMATSimTripScoringConfigGroup();
		return new LimaTripScoringEstimator(facilities, tripRouter, waitingTimeEstimator, scoringParametersForPerson,
				config.getPtLegModes(), limaScoringParametersForPerson);
	}
}
