package ch.ethz.matsim.lima_poc3.accessibility.travel_time_calculation;

import java.util.Iterator;
import java.util.List;

import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;

public class AccessibilityTravelTimeCalculationTaskGenerator
		implements Iterator<AccessibilityTravelTimeCalculationTask> {
	private final List<AccessibilityZone> zones;
	private int index = 0;

	public AccessibilityTravelTimeCalculationTaskGenerator(List<AccessibilityZone> zones) {
		this.zones = zones;
	}

	@Override
	public boolean hasNext() {
		return index < zones.size() * zones.size();
	}

	@Override
	public AccessibilityTravelTimeCalculationTask next() {
		int fromIndex = index / zones.size();
		int toIndex = index % zones.size();
		index++;

		return new AccessibilityTravelTimeCalculationTask(zones.get(fromIndex), zones.get(toIndex));
	}

	public int getProgress() {
		return index;
	}
}
