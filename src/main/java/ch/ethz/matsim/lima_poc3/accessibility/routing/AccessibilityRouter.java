package ch.ethz.matsim.lima_poc3.accessibility.routing;

import org.matsim.api.core.v01.Coord;

public interface AccessibilityRouter {
	double computeTravelTime(Coord fromCoord, Coord toCoord);
	
	interface Factory {
		AccessibilityRouter create();
	}
}
