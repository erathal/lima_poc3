package ch.ethz.matsim.lima_poc3.accessibility.zones;

import org.matsim.api.core.v01.Coord;

public class AccessibilityZone {
	public final int index;
	public final double opportunities;
	public final Coord coordinate;
	public final String identifier;

	public AccessibilityZone(int index, String identifier, double opportunities, Coord coordinate) {
		this.index = index;
		this.opportunities = opportunities;
		this.coordinate = coordinate;
		this.identifier = identifier;
	}
}
