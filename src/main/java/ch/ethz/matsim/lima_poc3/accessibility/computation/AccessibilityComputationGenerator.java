package ch.ethz.matsim.lima_poc3.accessibility.computation;

import java.util.Iterator;
import java.util.List;

import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;

public class AccessibilityComputationGenerator implements Iterator<AccessibilityZone> {
	private final List<AccessibilityZone> zones;
	private int currentIndex = 0;

	public AccessibilityComputationGenerator(List<AccessibilityZone> zones) {
		this.zones = zones;
	}

	@Override
	public boolean hasNext() {
		return currentIndex < zones.size();
	}

	@Override
	public AccessibilityZone next() {
		int returnIndex = currentIndex;
		currentIndex++;
		return zones.get(returnIndex);
	}

}
