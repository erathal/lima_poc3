package ch.ethz.matsim.lima_poc3.accessibility.zones;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureIterator;
import org.geotools.data.simple.SimpleFeatureSource;
import org.matsim.api.core.v01.Coord;
import org.opengis.feature.simple.SimpleFeature;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.Point;

public class AccessibilityZoneFactory {
	private final String opportunitiesAttribute;
	private final String identifierAttribute;

	public AccessibilityZoneFactory(String identifierAttribute, String opportunitiesAttribute) {
		this.opportunitiesAttribute = opportunitiesAttribute;
		this.identifierAttribute = identifierAttribute;
	}

	public List<AccessibilityZone> createZones(File shapefile) throws MalformedURLException, IOException {
		DataStore dataStore = DataStoreFinder.getDataStore(Collections.singletonMap("url", shapefile.toURI().toURL()));
		SimpleFeatureSource featureSource = dataStore.getFeatureSource(dataStore.getTypeNames()[0]);
		SimpleFeatureCollection featureCollection = featureSource.getFeatures();
		SimpleFeatureIterator featureIterator = featureCollection.features();

		List<AccessibilityZone> zones = new LinkedList<>();

		while (featureIterator.hasNext()) {
			SimpleFeature feature = featureIterator.next();
			Geometry geometry = (Geometry) feature.getDefaultGeometry();

			Point centroid = geometry.getCentroid();
			Coord coordinate = new Coord(centroid.getCoordinate().x, centroid.getCoordinate().y);
			
			double opportunities = (double) feature.getAttribute(opportunitiesAttribute);
			String identifier = String.valueOf(feature.getAttribute(identifierAttribute));

			zones.add(new AccessibilityZone(zones.size(), identifier, opportunities, coordinate));
		}
		
		featureIterator.close();
		dataStore.dispose();

		return zones;
	}
}
