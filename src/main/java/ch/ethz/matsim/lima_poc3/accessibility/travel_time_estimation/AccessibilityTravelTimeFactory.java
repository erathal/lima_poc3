package ch.ethz.matsim.lima_poc3.accessibility.travel_time_estimation;

import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.events.EventsUtils;
import org.matsim.core.events.MatsimEventsReader;
import org.matsim.core.router.util.TravelTime;

public class AccessibilityTravelTimeFactory {
	private final double averagingStartTime;
	private final double averagingEndTime;
	private final double freespeedFactor;

	public AccessibilityTravelTimeFactory(double averagingStartTime, double averagingEndTime, double freespeedFactor) {
		this.averagingStartTime = averagingStartTime;
		this.averagingEndTime = averagingEndTime;
		this.freespeedFactor = freespeedFactor;
	}

	public TravelTime create(String eventsPath) {
		EventsManager eventsManager = EventsUtils.createEventsManager();
		AccessibilityTravelTimeListener listener = new AccessibilityTravelTimeListener(averagingStartTime,
				averagingEndTime);
		eventsManager.addHandler(listener);
		new MatsimEventsReader(eventsManager).readFile(eventsPath);
		return new AccessibilityTravelTime(listener.getAverageTravelTimes(), freespeedFactor);
	}
}
