package ch.ethz.matsim.lima_poc3.accessibility.computation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityFunction;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityProgress;
import ch.ethz.matsim.lima_poc3.accessibility.utils.AccessibilityTravelTimeMatrix;
import ch.ethz.matsim.lima_poc3.accessibility.zones.AccessibilityZone;

public class AccessibilityComputationRunner implements Runnable {
	private final double[] accessibility;

	private final AccessibilityTravelTimeMatrix matrix;
	private final AccessibilityProgress progress;
	private final AccessibilityComputationGenerator generator;
	private final AccessibilityFunction accessibilityFunction;
	private final List<AccessibilityZone> zones;
	private final Function<List<Double>, Double> aggregationFunction;

	public AccessibilityComputationRunner(double[] accessibility, List<AccessibilityZone> zones,
			AccessibilityFunction accessibilityFunction, Function<List<Double>, Double> aggregationFunction,
			AccessibilityTravelTimeMatrix matrix, AccessibilityComputationGenerator generator,
			AccessibilityProgress progress) {
		this.accessibility = accessibility;
		this.matrix = matrix;
		this.progress = progress;
		this.generator = generator;
		this.zones = zones;
		this.accessibilityFunction = accessibilityFunction;
		this.aggregationFunction = aggregationFunction;
	}

	@Override
	public void run() {
		while (true) {
			AccessibilityZone originZone = null;

			synchronized (generator) {
				if (generator.hasNext()) {
					originZone = generator.next();
				} else {
					return;
				}
			}

			List<Double> localAccessibilities = new ArrayList<>(zones.size());

			for (AccessibilityZone destinationZone : zones) {
				double travelTime = matrix.get(originZone.index, destinationZone.index);
				localAccessibilities
						.add(accessibilityFunction.computeAccessibility(originZone, destinationZone, travelTime));
				progress.update();
			}

			accessibility[originZone.index] = aggregationFunction.apply(localAccessibilities);
		}
	}
}
