package ch.ethz.matsim.lima_poc3.accessibility.routing;

import org.matsim.api.core.v01.Coord;

public class AVAccessibilityRouter implements AccessibilityRouter {
	private final CarAccessibilityRouter carRouter;
	private final double offsetWaitingTime;

	public AVAccessibilityRouter(CarAccessibilityRouter carRouter, double offsetWaitingTime) {
		this.carRouter = carRouter;
		this.offsetWaitingTime = offsetWaitingTime;
	}

	@Override
	public double computeTravelTime(Coord fromCoord, Coord toCoord) {
		return carRouter.computeTravelTime(fromCoord, toCoord) + offsetWaitingTime;
	}

	public static class Factory implements AccessibilityRouter.Factory {
		private final CarAccessibilityRouter.Factory carFactory;
		private final double offsetWaitingTime;

		public Factory(CarAccessibilityRouter.Factory carFactory, double offsetWaitingTime) {
			this.carFactory = carFactory;
			this.offsetWaitingTime = offsetWaitingTime;
		}

		@Override
		public AVAccessibilityRouter create() {
			CarAccessibilityRouter carRouter = carFactory.create();
			return new AVAccessibilityRouter(carRouter, offsetWaitingTime);
		}
	}
}
