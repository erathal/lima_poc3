package ch.ethz.matsim.lima_poc3.accessibility.utils;

public class AccessibilityTravelTimeMatrix {
	private final double matrix[][];

	public AccessibilityTravelTimeMatrix(int numberOfZones) {
		matrix = new double[numberOfZones][numberOfZones];
	}

	public void set(int fromZone, int toZone, double travelTime) {
		matrix[fromZone][toZone] = travelTime;
	}

	public double get(int fromZone, int toZone) {
		return matrix[fromZone][toZone];
	}
}
