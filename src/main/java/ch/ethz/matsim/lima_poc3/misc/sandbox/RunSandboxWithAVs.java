package ch.ethz.matsim.lima_poc3.misc.sandbox;

import org.matsim.api.core.v01.Scenario;
import org.matsim.core.config.Config;
import org.matsim.core.controler.Controler;

public class RunSandboxWithAVs {
	static public void main(String[] args) {
		SandboxFactory factory = new SandboxFactory();

		// COMMENT OUT FOR A CLEAN START
		factory.setPopulation("output_raptor/output_plans.xml.gz");

		Scenario scenario = factory.create();
		SandboxUtils.updateScenarioWithAVs(scenario);

		Config config = scenario.getConfig();
		config.controler().setOutputDirectory("output_with_avs2");
		config.controler().setLastIteration(200);

		Controler controler = new Controler(scenario);
		SandboxUtils.updateControllerWithAVs(controler, false);

		controler.run();
	}
}
