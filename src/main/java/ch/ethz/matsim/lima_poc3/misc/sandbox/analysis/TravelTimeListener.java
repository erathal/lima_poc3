package ch.ethz.matsim.lima_poc3.misc.sandbox.analysis;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.api.core.v01.events.handler.PersonArrivalEventHandler;
import org.matsim.api.core.v01.events.handler.PersonDepartureEventHandler;
import org.matsim.api.core.v01.events.handler.PersonEntersVehicleEventHandler;
import org.matsim.api.core.v01.population.Person;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.events.ShutdownEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.controler.listener.ShutdownListener;

public class TravelTimeListener implements PersonArrivalEventHandler, PersonDepartureEventHandler,
		PersonEntersVehicleEventHandler, IterationEndsListener, ShutdownListener {
	private final BufferedWriter writer;
	private final Map<String, Double> travelTimes = new HashMap<>();
	private final List<String> modes;

	private final Map<Id<Person>, PersonDepartureEvent> departureEvents = new HashMap<>();
	private final Map<Id<Person>, PersonEntersVehicleEvent> enterEvents = new HashMap<>();

	public TravelTimeListener(OutputDirectoryHierarchy output, Set<String> modes) {
		this.modes = new ArrayList<>(modes);

		try {
			String outputPath = output.getOutputFilename("travel_time.csv");

			writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(outputPath)));
			writer.write("iteration;" + String.join(";", this.modes) + "\n");
			writer.flush();

			for (String mode : modes) {
				travelTimes.put(mode, 0.0);
			}
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void handleEvent(PersonDepartureEvent event) {
		if (modes.contains(event.getLegMode())) {
			departureEvents.put(event.getPersonId(), event);
		}
	}

	@Override
	public void handleEvent(PersonEntersVehicleEvent enterEvent) {
		PersonDepartureEvent departureEvent = departureEvents.get(enterEvent.getPersonId());

		if (departureEvent != null) {
			enterEvents.put(enterEvent.getPersonId(), enterEvent);
		}
	}

	@Override
	public void handleEvent(PersonArrivalEvent arrivalEvent) {
		PersonDepartureEvent departureEvent = departureEvents.remove(arrivalEvent.getPersonId());
		PersonEntersVehicleEvent enterEvent = enterEvents.remove(arrivalEvent.getPersonId());

		if (departureEvent != null && enterEvent != null) {
			if (departureEvent.getLegMode().equals(arrivalEvent.getLegMode())) {
				double travelTime = arrivalEvent.getTime() - enterEvent.getTime();
				travelTimes.put(departureEvent.getLegMode(), travelTimes.get(departureEvent.getLegMode()) + travelTime);
			}
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		try {
			List<String> printTravelTimes = new LinkedList<>();

			for (String mode : modes) {
				printTravelTimes.add(String.valueOf(travelTimes.get(mode)));
				travelTimes.put(mode, 0.0);
			}

			departureEvents.clear();
			enterEvents.clear();

			writer.write(String.valueOf(event.getIteration()) + ";" + String.join(";", printTravelTimes) + "\n");
			writer.flush();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public void notifyShutdown(ShutdownEvent event) {
		try {
			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
