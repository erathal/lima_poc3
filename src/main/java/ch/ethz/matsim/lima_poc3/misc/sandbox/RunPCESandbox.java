package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.util.Random;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.config.groups.QSimConfigGroup.VehiclesSource;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.vehicles.VehicleType;

import com.google.inject.Key;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.lima_poc3.flow_efficiency.ConstantFlowEfficiency;
import ch.ethz.matsim.lima_poc3.flow_efficiency.FlowEfficiency;
import ch.ethz.matsim.lima_poc3.flow_efficiency.LimaFlowEfficiencyModule;
import ch.ethz.matsim.lima_poc3.flow_efficiency.LimaVehicleType;
import ch.ethz.matsim.lima_poc3.misc.sandbox.analysis.TravelTimeListenerModule;
import ch.ethz.matsim.lima_poc3.misc.sandbox.analysis.TripCountListenerModule;

public class RunPCESandbox {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args).allowOptions("av-flow-efficiency", "car-flow-efficiency",
				"initial-share", "iterations", "output", "flow-capacity-factor").build();

		double avFlowEfficiency = cmd.getOption("av-flow-efficiency").map(Double::parseDouble).orElse(1.0);
		double carFlowEfficiency = cmd.getOption("car-flow-efficiency").map(Double::parseDouble).orElse(1.0);
		double initialShare = cmd.getOption("initial-share").map(Double::parseDouble).orElse(0.0);
		int iterations = cmd.getOption("iterations").map(Integer::parseInt).orElse(500);
		String output = cmd.getOption("output").orElse("output_pce");
		double flowCapacityFactor = cmd.getOption("flow-capacity-factor").map(Double::parseDouble).orElse(1.0);

		SandboxFactory factory = new SandboxFactory();
		factory.setEnableModeChoice(false);

		Scenario scenario = factory.create();
		SandboxUtils.updateScenarioWithAVs(scenario);

		Config config = scenario.getConfig();

		config.controler().setOutputDirectory(output);
		config.controler().setLastIteration(iterations);
		config.qsim().setFlowCapFactor(flowCapacityFactor);
		config.qsim().setStorageCapFactor(10000.0);

		Random random = new Random(0);

		for (Person person : scenario.getPopulation().getPersons().values()) {
			if (random.nextDouble() < initialShare) {
				for (Plan plan : person.getPlans()) {
					for (PlanElement element : plan.getPlanElements()) {
						if (element instanceof Leg) {
							Leg leg = (Leg) element;
							leg.setMode("av");
							leg.setRoute(null);
						}
					}
				}
			}
		}

		config.qsim().setVehiclesSource(VehiclesSource.modeVehicleTypesFromVehiclesData);

		LimaVehicleType carType = new LimaVehicleType(new ConstantFlowEfficiency(carFlowEfficiency),
				Id.create("car", VehicleType.class));
		scenario.getVehicles().addVehicleType(carType);

		Controler controler = new Controler(scenario);
		controler.addOverridingModule(new DvrpTravelTimeModule());
		controler.addOverridingModule(new AVModule());
		controler.addOverridingModule(new LimaFlowEfficiencyModule());

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				AVConfig config = new AVConfig();

				AVOperatorConfig operator = config.createOperatorConfig("av");
				operator.createDispatcherConfig("SingleHeuristic");
				operator.createGeneratorConfig("PopulationDensity").setNumberOfVehicles(100);
				operator.createPriceStructureConfig();

				bind(AVConfig.class).toInstance(config);
				bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
						.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();

				install(new TripCountListenerModule("av", "car"));
				install(new TravelTimeListenerModule("av", "car"));
			}

		});

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				bind(FlowEfficiency.class).toInstance(new ConstantFlowEfficiency(avFlowEfficiency));
			}
		});

		controler.run();
	}
}
