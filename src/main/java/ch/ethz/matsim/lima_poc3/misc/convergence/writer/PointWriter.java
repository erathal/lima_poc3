package ch.ethz.matsim.lima_poc3.misc.convergence.writer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

import ch.ethz.matsim.lima_poc3.misc.convergence.variables.PointVariable;

public class PointWriter implements ConvergenceWriter {
	private final File path;
	private final PointVariable variable;
	private boolean writeHeader = true;

	public PointWriter(File basePath, PointVariable variable) {
		this.path = new File(basePath, "var_" + variable.getName() + ".csv");
		this.variable = variable;
	}

	@Override
	public void write(int iteration) throws IOException {
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path, true)));

		if (writeHeader) {
			writeHeader = false;
			writer.write(String.join(";", new String[] { "iteration", "value" }) + "\n");
		}

		writer.write(String.join(";", new String[] { String.valueOf(iteration), String.valueOf(variable.getValue()) })
				+ "\n");
		writer.close();
	}
}
