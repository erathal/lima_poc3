package ch.ethz.matsim.lima_poc3.misc.sandbox;

import java.util.Arrays;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.lima_poc3.av.space.AttributeSpatialCapacity;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;
import ch.ethz.matsim.lima_poc3.misc.convergence.ConvergenceModule;
import ch.ethz.matsim.lima_poc3.misc.convergence.variables.repository.LegCountVariable;
import ch.ethz.matsim.lima_poc3.misc.sandbox.analysis.AVDistanceVariable;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;

public class RunSandboxWithAVsAndSpatialConstraints {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args)
				.allowOptions("direct", "spots", "parallel", "output", "use-parking-dispatcher", "iterations").build();

		boolean directCase = cmd.getOption("direct").map(Boolean::parseBoolean).orElse(false);
		long numberOfSpots = cmd.getOption("spots").map(Long::parseLong).orElse(200L);
		boolean allowParallelLinks = cmd.getOption("parallel").map(Boolean::parseBoolean).orElse(true);
		String outputPath = cmd.getOption("output").orElse("output_with_spatial_avs");
		boolean useParkingDispatcher = cmd.getOption("use-parking-dispatcher").map(Boolean::parseBoolean).orElse(false);
		int numberOfIterations = cmd.getOption("iterations").map(Integer::parseInt).orElse(400);

		SandboxFactory factory = new SandboxFactory();

		// COMMENT OUT FOR A CLEAN START
		// factory.setPopulation("output_raptor/output_plans.xml.gz");

		Scenario scenario = factory.create();
		SandboxUtils.updateScenarioWithAVs(scenario);
		SandboxUtils.updateScenarioWithIntermodalAVs(scenario);

		Config config = scenario.getConfig();
		config.controler().setOutputDirectory(outputPath);
		config.controler().setLastIteration(numberOfIterations);
		config.controler().setWriteEventsInterval(numberOfIterations);
		config.controler().setWritePlansInterval(numberOfIterations);

		for (Link link : scenario.getNetwork().getLinks().values()) {
			link.getAttributes().putAttribute("parkingCapacity", numberOfSpots);
			link.getAttributes().putAttribute("pickupDropoffCapacity", numberOfSpots);
		}

		if (allowParallelLinks) {
			TransitStopFacility stopA = scenario.getTransitSchedule().getFacilities()
					.get(Id.create("stopA", TransitStopFacility.class));

			stopA.getAttributes().putAttribute("avFeederLinkIds",
					stopA.getAttributes().getAttribute("avFeederLinkIds") + ",link_3_11_to_2_11");

			TransitStopFacility stopB = scenario.getTransitSchedule().getFacilities()
					.get(Id.create("stopB", TransitStopFacility.class));

			stopB.getAttributes().putAttribute("avFeederLinkIds",
					stopB.getAttributes().getAttribute("avFeederLinkIds") + ",link_0_2_to_1_2");
		}

		if (directCase) {
			((SwissRailRaptorConfigGroup) config.getModules().get(SwissRailRaptorConfigGroup.GROUP))
					.setUseIntermodalAccessEgress(false);
		} else {
			config.subtourModeChoice().setModes(new String[] { "car", "pt", "walk" });
		}

		Controler controler = new Controler(scenario);
		SandboxUtils.updateControllerWithAVs(controler, useParkingDispatcher);
		SandboxUtils.updateControllerWithRaptor(controler, false);
		SandboxUtils.updateControllerWithIntermodalAVs(controler);

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				bind(Key.get(SpatialCapacity.class, Names.named("parking")))
						.toInstance(new AttributeSpatialCapacity("parkingCapacity"));
				bind(Key.get(SpatialCapacity.class, Names.named("pickupDropoff")))
						.toInstance(new AttributeSpatialCapacity("pickupDropoffCapacity"));
			}
		});

		controler.addOverridingModule(new ConvergenceModule() {
			@Override
			public void installCriteria() {
				LegCountVariable legCount = new LegCountVariable(Arrays.asList("car", "pt", "av_direct", "walk"));

				addEventHandlerBinding().toInstance(legCount);
				addEventHandlerBinding().to(AVDistanceVariable.class);

				addVariable(AVDistanceVariable.class);
				addVariable(legCount);
			}

			@Singleton
			@Provides
			AVDistanceVariable provideAVDistanceVariable(@Named(AVModule.AV_MODE) Network network) {
				return new AVDistanceVariable(network);
			}
		});

		controler.run();
	}
}
