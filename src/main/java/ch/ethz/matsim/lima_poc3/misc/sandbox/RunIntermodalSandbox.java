package ch.ethz.matsim.lima_poc3.misc.sandbox;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;

import ch.ethz.matsim.baseline_scenario.config.CommandLine;
import ch.ethz.matsim.baseline_scenario.config.CommandLine.ConfigurationException;
import ch.ethz.matsim.lima_poc3.misc.sandbox.analysis.TripCountListenerModule;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;

public class RunIntermodalSandbox {
	static public void main(String[] args) throws ConfigurationException {
		CommandLine cmd = new CommandLine.Builder(args).allowOptions("fleet-size", "headway", "population-path",
				"output-path", "iterations", "use-intermodal", "beta-waiting-time").build();

		double headway = cmd.getOption("headway").map(Double::parseDouble).orElse(300.0);
		String outputPath = cmd.getOption("output-path").orElse("output_intermodal");
		int iterations = cmd.getOption("iterations").map(Integer::parseInt).orElse(200);
		boolean useIntermodal = cmd.getOption("use-intermodal").map(Boolean::parseBoolean).orElse(true);
		double betaWaitingTime = cmd.getOption("beta-waiting-time").map(Double::parseDouble).orElse(0.0);

		SandboxFactory factory = new SandboxFactory();

		if (cmd.hasOption("population-path")) {
			factory.setPopulation(cmd.getOptionStrict("population-path"));
		}
		factory.setHeadway(headway);

		Scenario scenario = factory.create();
		SandboxUtils.updateScenarioWithAVs(scenario);
		SandboxUtils.updateScenarioWithIntermodalAVs(scenario);

		Config config = scenario.getConfig();
		config.controler().setLastIteration(iterations);
		config.controler().setOutputDirectory(outputPath);
		config.planCalcScore().setMarginalUtlOfWaitingPt_utils_hr(betaWaitingTime);
		((SwissRailRaptorConfigGroup) config.getModules().get(SwissRailRaptorConfigGroup.GROUP))
				.setUseIntermodalAccessEgress(useIntermodal);

		if (true) { // Only allow AVs on the lower side
			for (Link link : scenario.getNetwork().getLinks().values()) {
				if (link.getCoord().getY() > 5000) {
					link.getAttributes().putAttribute("isAvailableForAVInteraction", false);
				}
			}

			for (TransitStopFacility facility : scenario.getTransitSchedule().getFacilities().values()) {
				facility.getAttributes().removeAttribute("avFeederLinkIds");
			}

			((SwissRailRaptorConfigGroup) config.getModules().get(SwissRailRaptorConfigGroup.GROUP))
					.setUseIntermodalAccessEgress(false);
		}

		Controler controler = new Controler(scenario);
		SandboxUtils.updateControllerWithAVs(controler, false);
		SandboxUtils.updateControllerWithRaptor(controler, false);
		SandboxUtils.updateControllerWithIntermodalAVs(controler);

		controler.addOverridingModule(new AbstractModule() {
			@Override
			public void install() {
				install(new TripCountListenerModule("pt"));
			}
		});

		controler.run();
	}
}
