package ch.ethz.matsim.lima_poc3.misc.sandbox.analysis;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class ConvergenceWriter {
	private final List<Double> values = new LinkedList<>();

	private final List<Double> means = new LinkedList<>();
	private final List<Double> standardDeviations = new LinkedList<>();
	private final List<Double> meanOfTheMeans = new LinkedList<>();
	private final List<Double> standardDeviationOfTheMeans = new LinkedList<>();

	private final BufferedWriter writer;

	public ConvergenceWriter(BufferedWriter writer) throws IOException {
		this.writer = writer;
	}

	public void addValue(double value) throws IOException {
		if (values.size() == 0) {
			writer.write(String.join(";", Arrays.asList(new String[] { "iteration", "value", "mean",
					"standardDeviation", "meanOfTheMean", "standardDeviationOfTheMean" })) + "\n");
		}

		values.add(value);

		// Compute new mean

		double mean = 0.0;

		for (int i = 0; i < values.size(); i++) {
			mean += values.get(i);
		}

		mean /= values.size();
		means.add(mean);

		// Compute new standard deviation

		double standardDeviation = 0.0;

		for (int i = 0; i < values.size(); i++) {
			standardDeviation += Math.pow(values.get(i) - mean, 2.0);
		}

		standardDeviation /= values.size() - 1;
		standardDeviation = Math.sqrt(standardDeviation);
		standardDeviations.add(standardDeviation);
		standardDeviations.set(0, Double.NaN);

		// Compute mean of the mean

		double meanOfTheMean = 0.0;

		for (int i = 0; i < values.size(); i++) {
			meanOfTheMean += means.get(i);
		}

		meanOfTheMean /= values.size();
		meanOfTheMeans.add(meanOfTheMean);

		// Compute standard deviation of the mean

		double standardDeviationOfTheMean = 0.0;

		for (int i = 1; i < values.size(); i++) {
			standardDeviationOfTheMean += Math.pow(means.get(i) - meanOfTheMean, 2.0);
		}

		standardDeviationOfTheMean /= values.size() - 1;
		standardDeviationOfTheMean = Math.sqrt(standardDeviationOfTheMean);
		standardDeviationOfTheMeans.add(standardDeviationOfTheMean);
		standardDeviationOfTheMeans.set(0, Double.NaN);

		writer.write(String.join(";",
				Arrays.asList(new String[] { String.valueOf(values.size()), String.valueOf(value), String.valueOf(mean),
						String.valueOf(standardDeviation), String.valueOf(meanOfTheMean),
						String.valueOf(standardDeviationOfTheMean) }))
				+ "\n");
		writer.flush();
	}

}
