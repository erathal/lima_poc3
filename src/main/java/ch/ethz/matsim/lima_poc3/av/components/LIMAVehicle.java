package ch.ethz.matsim.lima_poc3.av.components;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.contrib.dvrp.data.Vehicle;

import ch.ethz.matsim.av.data.AVVehicle;

public class LIMAVehicle extends AVVehicle {
	public LIMAVehicle(Id<Vehicle> id, Link startLink, double capacity, double t0, double t1) {
		super(id, startLink, capacity, t0, t1);
	}

	public enum Intention {
		Parking, PickupDropoff
	}

	private Intention intention = Intention.Parking;

	public Intention getIntention() {
		return intention;
	}

	public void setIntention(Intention intention) {
		this.intention = intention;
	}
}
