package ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking;

import org.matsim.api.core.v01.network.Link;

import ch.ethz.matsim.av.data.AVVehicle;

public class BlockingVehicleWrapper {
	final Link link;
	final AVVehicle vehicle;

	public BlockingVehicleWrapper(Link link, AVVehicle vehicle) {
		this.link = link;
		this.vehicle = vehicle;
	}
}