package ch.ethz.matsim.lima_poc3.av.routing.temporal;

import org.matsim.facilities.Facility;

public interface AVInteractionTime {
	double getPickupTime(Facility<?> facility, double time, double approachTime);
	double getDropoffTime(Facility<?> facility, double time);
}
