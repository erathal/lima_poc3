package ch.ethz.matsim.lima_poc3.av.routing.temporal.estimation;

public interface IndexEstimator {
	void record(int itemIndex, int timeIndex, double value);

	double estimate(int itemIndex, int timeIndex);

	void finish();
}
