package ch.ethz.matsim.lima_poc3.av.space;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.matsim.api.core.v01.network.Link;
import org.matsim.core.mobsim.qsim.qnetsimengine.QVehicle;

public interface VehicleInteractionHandler {
	void registerParkedVehicle(QVehicle vehicle, Link link);

	boolean handleArrival(QVehicle vehicle, Link link);

	void handleDeparture(QVehicle vehicle, Link link);

	public static final InstanceVehicleInteractionHandler INSTANCE = new InstanceVehicleInteractionHandler();

	public final class InstanceVehicleInteractionHandler implements VehicleInteractionHandler {
		private final static Logger logger = Logger.getLogger(InstanceVehicleInteractionHandler.class);

		static {
			logger.setLevel(Level.OFF);
		}

		private VehicleInteractionHandler delegate = null;

		public void setDelegate(VehicleInteractionHandler delegate) {
			this.delegate = delegate;
		}

		public void enableLogger() {
			logger.setLevel(Level.ALL);
		}

		@Override
		public void registerParkedVehicle(QVehicle vehicle, Link link) {
			if (delegate == null) {
				logger.warn("No VehicleInteractionHandler defined.");
			} else {
				delegate.registerParkedVehicle(vehicle, link);
			}
		}

		@Override
		public boolean handleArrival(QVehicle vehicle, Link link) {
			if (delegate == null) {
				logger.warn("No VehicleInteractionHandler defined.");
				return true;
			} else {
				return delegate.handleArrival(vehicle, link);
			}
		}

		@Override
		public void handleDeparture(QVehicle vehicle, Link link) {
			if (delegate == null) {
				logger.warn("No VehicleInteractionHandler defined.");
			} else {
				delegate.handleDeparture(vehicle, link);
			}
		}
	};
}
