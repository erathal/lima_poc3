package ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking;

import java.util.concurrent.Future;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;

import ch.ethz.matsim.av.data.AVVehicle;

public class BlockingVehicleAppendTask {
	public final AVVehicle vehicle;

	public final Future<Path> pathFuture;

	public final Link originLink;
	public final Link destinationLink;

	public BlockingVehicleAppendTask(AVVehicle vehicle, Link originLink, Link destinationLink,
			Future<Path> pathFuture) {
		this.originLink = originLink;
		this.destinationLink = destinationLink;
		this.vehicle = vehicle;
		this.pathFuture = pathFuture;
	}
}
