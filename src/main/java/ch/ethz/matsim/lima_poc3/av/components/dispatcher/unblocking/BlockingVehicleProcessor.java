package ch.ethz.matsim.lima_poc3.av.components.dispatcher.unblocking;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import org.matsim.api.core.v01.Coord;
import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.path.VrpPathWithTravelData;
import org.matsim.contrib.dvrp.path.VrpPaths;
import org.matsim.contrib.dvrp.schedule.Schedule;
import org.matsim.contrib.dvrp.schedule.Schedules;
import org.matsim.contrib.dvrp.schedule.Task;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;

import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.plcpc.ParallelLeastCostPathCalculator;
import ch.ethz.matsim.av.schedule.AVDriveTask;
import ch.ethz.matsim.av.schedule.AVStayTask;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

public class BlockingVehicleProcessor {
	private final Map<Link, SpatialLink> spatialLinks = new HashMap<>();

	private final Network network;
	private final TravelTime travelTime;
	private final ParallelLeastCostPathCalculator router;

	private final List<BlockingVehicleAppendTask> tasks = new LinkedList<>();

	private final SpatialCapacity spatialCapacity;

	public BlockingVehicleProcessor(Network network, SpatialCapacity spatialCapacity,
			ParallelLeastCostPathCalculator router, TravelTime travelTime) {
		this.network = network;
		this.router = router;
		this.travelTime = travelTime;

		for (Id<Link> linkId : network.getLinks().keySet()) {
			Link link = network.getLinks().get(linkId);
			spatialLinks.put(link, new SpatialLink(link));
		}

		this.spatialCapacity = spatialCapacity;
	}

	public void registerApproachingVehicle(Link link, AVVehicle vehicle) {
		// logger.warn(String.format("register approaching vehicle %s @ %s",
		// vehicle.getId().toString(),
		// link.getId().toString()));
		spatialLinks.get(link).addApproachingVehicle(vehicle);
	}

	public void removeApproachingVehicle(Link link, AVVehicle vehicle) {
		// logger.warn(String.format("remove approaching vehicle %s @ %s",
		// vehicle.getId().toString(),
		// link.getId().toString()));
		spatialLinks.get(link).removeApproachingVehicle(vehicle);
	}

	public void registerIdleVehicle(Link link, AVVehicle vehicle) {
		// logger.warn(
		// String.format("register idle vehicle %s @ %s", vehicle.getId().toString(),
		// link.getId().toString()));
		spatialLinks.get(link).addIdleVehicle(vehicle);
	}

	public void removeIdleVehicle(Link link, AVVehicle vehicle) {
		// logger.warn(String.format("remove idle vehicle %s @ %s",
		// vehicle.getId().toString(), link.getId().toString()));
		spatialLinks.get(link).removeIdleVehicle(vehicle);
	}

	public Collection<AVVehicle> dispatch(double now) {
		QuadTree<AvailableLinkWrapper> availableLinkTree = createAvailableLinkTree();

		long numberOfAvailableSpots = 0;
		List<BlockingVehicleWrapper> blockingVehicles = new LinkedList<>();

		for (SpatialLink spatialLink : spatialLinks.values()) {
			long numberOfSpots = spatialCapacity.getSpatialCapacity(spatialLink.getLink(), now);

			if (spatialLink.getExcessSpots(numberOfSpots) > 0) {
				Coord coord = spatialLink.getLink().getCoord();
				availableLinkTree.put(coord.getX(), coord.getY(),
						new AvailableLinkWrapper(spatialLink.getLink(), numberOfAvailableSpots));
				numberOfAvailableSpots += spatialLink.getExcessSpots(numberOfSpots);
			} else {
				blockingVehicles.addAll(spatialLink.getExcessVehicles(numberOfSpots).stream()
						.map(v -> new BlockingVehicleWrapper(spatialLink.getLink(), v)).collect(Collectors.toList()));
			}
		}

		if (blockingVehicles.size() > numberOfAvailableSpots) {
			throw new IllegalStateException();
		}

		for (BlockingVehicleWrapper blockingVehicle : blockingVehicles) {
			AVVehicle vehicle = blockingVehicle.vehicle;
			Coord idleCoord = blockingVehicle.link.getCoord();

			AvailableLinkWrapper availableLink = availableLinkTree.getClosest(idleCoord.getX(), idleCoord.getY());
			Link destinationLink = availableLink.link;
			availableLink.numberOfAvailableSpots--;

			if (availableLink.numberOfAvailableSpots == 0) {
				availableLinkTree.remove(destinationLink.getCoord().getX(), destinationLink.getCoord().getY(),
						availableLink);
			}

			Future<Path> pathFuture = router.calcLeastCostPath(blockingVehicle.link.getToNode(),
					destinationLink.getFromNode(), now, null, null);

			BlockingVehicleAppendTask task = new BlockingVehicleAppendTask(vehicle, blockingVehicle.link,
					destinationLink, pathFuture);
			tasks.add(task);

			// logger.warn(String.format("Moving away %s from %s to %s",
			// vehicle.getId().toString(),
			// blockingVehicle.link.getId().toString(),
			// destinationLink.getId().toString()));

			registerApproachingVehicle(destinationLink, vehicle);
		}

		return blockingVehicles.stream().map(b -> b.vehicle).collect(Collectors.toList());
	}

	public void update(double now) {
		try {
			for (BlockingVehicleAppendTask task : tasks) {
				AVVehicle vehicle = task.vehicle;

				Schedule schedule = vehicle.getSchedule();
				AVStayTask stayTask = (AVStayTask) Schedules.getLastTask(schedule);

				double startTime = 0.0;
				double scheduleEndTime = schedule.getEndTime();

				if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
					startTime = now;
				} else {
					startTime = stayTask.getBeginTime();
				}

				Path path = task.pathFuture.get();

				VrpPathWithTravelData vrpPath = VrpPaths.createPath(task.originLink, task.destinationLink, startTime,
						path, travelTime);
				AVDriveTask driveTask = new AVDriveTask(vrpPath);

				if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
					stayTask.setEndTime(startTime);
				} else {
					schedule.removeLastTask();
				}

				schedule.addTask(driveTask);

				if (driveTask.getEndTime() < scheduleEndTime) {
					schedule.addTask(new AVStayTask(driveTask.getEndTime(), scheduleEndTime, task.destinationLink));
				}
			}

			tasks.clear();
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}
	}

	private QuadTree<AvailableLinkWrapper> createAvailableLinkTree() {
		double[] bounds = NetworkUtils.getBoundingBox(network.getNodes().values()); // minx, miny, maxx, maxy
		return new QuadTree<>(bounds[0], bounds[1], bounds[2], bounds[3]);
	}
}
