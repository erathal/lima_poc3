package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;
import org.matsim.core.utils.misc.Time;

public class LimaInteractionTimeParameterSet extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "InteractionTime:Lima";

	public final static String ESTIMATION_START_TIME = "estimationStartTime";
	public final static String ESTIMATION_END_TIME = "estimationEndTime";
	public final static String ESTIMATION_INTERVAL = "estimationInterval";
	public final static String HORIZON = "horizon";
	public final static String DEFAULT_WAITING_TIME = "defaultWaitingTime_s";
	public final static String LINK_ATTRIBUTE = "linkAttribute";

	private double estimationStartTime = 5.0 * 3600.0;
	private double estimationEndTime = 22.0 * 3600.0;
	private double estimationInterval = 300.0;

	private int horizon = 10;
	private double defaultWaitingTime = 300.0;

	private String linkAttribute = "avWaitingTimeGroup";

	public LimaInteractionTimeParameterSet() {
		super(GROUP_NAME);
	}

	public double getEstimationStartTime() {
		return estimationStartTime;
	}

	public void setEstimationStartTime(double estimationStartTime) {
		this.estimationStartTime = estimationStartTime;
	}

	@StringGetter(ESTIMATION_START_TIME)
	public String getEstimationStartTimeAsString() {
		return Time.writeTime(estimationStartTime);
	}

	@StringSetter(ESTIMATION_START_TIME)
	public void setEstimationStartTimeAsString(String estimationStartTime) {
		this.estimationStartTime = Time.parseTime(estimationStartTime);
	}

	public double getEstimationEndTime() {
		return estimationEndTime;
	}

	public void setEstimationEndTime(double estimationEndTime) {
		this.estimationEndTime = estimationEndTime;
	}

	@StringGetter(ESTIMATION_END_TIME)
	public String getEstimationEndTimeAsString() {
		return Time.writeTime(estimationEndTime);
	}

	@StringSetter(ESTIMATION_END_TIME)
	public void setEstimationEndTimeAsString(String estimationEndTime) {
		this.estimationEndTime = Time.parseTime(estimationEndTime);
	}

	public double getEstimationInterval() {
		return estimationInterval;
	}

	public void setEstimationInterval(double estimationInterval) {
		this.estimationInterval = estimationInterval;
	}

	@StringGetter(ESTIMATION_INTERVAL)
	public String getEstimationIntervalAsString() {
		return Time.writeTime(estimationInterval);
	}

	@StringSetter(ESTIMATION_INTERVAL)
	public void setEstimationIntervalAsString(String estimationInterval) {
		this.estimationInterval = Time.parseTime(estimationInterval);
	}

	@StringGetter(HORIZON)
	public int getHorizon() {
		return horizon;
	}

	@StringSetter(HORIZON)
	public void setHorizon(int horizon) {
		this.horizon = horizon;
	}

	@StringGetter(DEFAULT_WAITING_TIME)
	public double getDefaultWaitingTime() {
		return defaultWaitingTime;
	}

	@StringSetter(DEFAULT_WAITING_TIME)
	public void setDefaultWaitingTime(double defaultWaitingTime) {
		this.defaultWaitingTime = defaultWaitingTime;
	}

	@StringGetter(LINK_ATTRIBUTE)
	public String getLinkAttribute() {
		return linkAttribute;
	}

	@StringSetter(LINK_ATTRIBUTE)
	public void setLinkAttribute(String linkAttribute) {
		this.linkAttribute = linkAttribute;
	}
}
