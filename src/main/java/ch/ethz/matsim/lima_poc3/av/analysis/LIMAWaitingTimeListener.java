package ch.ethz.matsim.lima_poc3.av.analysis;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.controler.events.IterationEndsEvent;
import org.matsim.core.controler.listener.IterationEndsListener;
import org.matsim.core.router.LinkWrapperFacility;
import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.lima_poc3.av.config.LimaInteractionTimeParameterSet;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.estimation.LIMAInteractionTime;

public class LIMAWaitingTimeListener implements IterationEndsListener {
	private final OutputDirectoryHierarchy outputDirectoryHierarchy;
	private final LIMAInteractionTime interactionTime;
	private final LimaInteractionTimeParameterSet config;

	private final Map<String, Link> testLinks = new HashMap<>();

	public LIMAWaitingTimeListener(LimaInteractionTimeParameterSet config, LIMAInteractionTime interactionTime,
			Network network, OutputDirectoryHierarchy outputDirectoryHierarchy) {
		this.interactionTime = interactionTime;
		this.config = config;
		this.outputDirectoryHierarchy = outputDirectoryHierarchy;

		for (Link link : network.getLinks().values()) {
			String group = (String) link.getAttributes().getAttribute(config.getLinkAttribute());

			if (group != null && !testLinks.containsKey(group)) {
				testLinks.put(group, link);
			}
		}
	}

	@Override
	public void notifyIterationEnds(IterationEndsEvent event) {
		try {
			double startTime = config.getEstimationStartTime();
			double endTime = config.getEstimationEndTime();
			double interval = config.getEstimationInterval();

			String path = outputDirectoryHierarchy.getIterationFilename(event.getIteration(), "lima_waiting_time.csv");
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path)));

			double time = startTime;

			writer.write("link_id");
			while (time <= endTime) {
				writer.write(";" + Time.writeTime(time));
			}
			writer.write("\n");

			for (Map.Entry<String, Link> entry : testLinks.entrySet()) {
				String groupName = entry.getKey();
				Link testLink = entry.getValue();

				time = startTime;

				writer.write(groupName);
				while (time <= endTime) {
					double waitingTime = interactionTime.getPickupTime(new LinkWrapperFacility(testLink), endTime, 0.0);
					writer.write(";" + String.valueOf(waitingTime));
					time += interval;
				}
				writer.write("\n");
			}

			writer.close();
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
