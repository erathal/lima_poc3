package ch.ethz.matsim.lima_poc3.av;

import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.core.controler.AbstractModule;

import com.google.inject.Key;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.config.AVOperatorConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.av.framework.AVUtils;
import ch.ethz.matsim.lima_poc3.av.analysis.AVNetworkListener;
import ch.ethz.matsim.lima_poc3.av.components.SpatiallyConstrainedPopulationDensityGenerator;
import ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking.ParkingSingleHeuristicDispatcher;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionLinkData;
import ch.ethz.matsim.lima_poc3.av.space.VehicleInteractionHandler;

public class LimaAvModule extends AbstractModule {
	@Override
	public void install() {
		LimaAvConfigGroup limaAvConfig = (LimaAvConfigGroup) getConfig().getModules().get(LimaAvConfigGroup.GROUP_NAME);
		VehicleInteractionHandler.INSTANCE.enableLogger();

		bind(NearestInteractionLinkData.class).asEagerSingleton();

		AVUtils.registerGeneratorFactory(binder(), "SpatiallyConstrainedPopulationDensity",
				SpatiallyConstrainedPopulationDensityGenerator.Factory.class);

		AVUtils.registerDispatcherFactory(binder(), "ParkingDispatcher",
				ParkingSingleHeuristicDispatcher.Factory.class);

		AVConfig avConfig = new AVConfig();
		AVOperatorConfig operator = avConfig.createOperatorConfig(AVModule.AV_MODE);
		operator.createDispatcherConfig("ParkingDispatcher");
		operator.createGeneratorConfig("SpatiallyConstrainedPopulationDensity")
				.setNumberOfVehicles(limaAvConfig.getFleetSize());
		operator.createPriceStructureConfig();

		bind(AVConfig.class).toInstance(avConfig);
		bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
				.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();

		addControlerListenerBinding().to(AVNetworkListener.class);
	}
}
