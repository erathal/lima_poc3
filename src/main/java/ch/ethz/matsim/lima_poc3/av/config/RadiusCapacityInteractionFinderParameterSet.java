package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class RadiusCapacityInteractionFinderParameterSet extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "InteractionPointFinder:CapacityAwareWithinRadius";

	public final static String SEARCH_RADIUS = "searchRadius";

	private double searchRadius = 150.0;

	public RadiusCapacityInteractionFinderParameterSet() {
		super(GROUP_NAME);
	}

	@StringGetter(SEARCH_RADIUS)
	public double getSearchRadius() {
		return searchRadius;
	}

	@StringSetter(SEARCH_RADIUS)
	public void setSearchRadius(double searchRadius) {
		this.searchRadius = searchRadius;
	}
}
