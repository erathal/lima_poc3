package ch.ethz.matsim.lima_poc3.av.simulation;

import java.util.List;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.population.Person;
import org.matsim.api.core.v01.population.Plan;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.core.mobsim.framework.MobsimAgent;
import org.matsim.core.mobsim.framework.PlanAgent;
import org.matsim.core.mobsim.qsim.interfaces.MobsimVehicle;
import org.matsim.core.mobsim.qsim.pt.MobsimDriverPassengerAgent;
import org.matsim.core.mobsim.qsim.pt.TransitVehicle;
import org.matsim.facilities.Facility;
import org.matsim.pt.transitSchedule.api.TransitLine;
import org.matsim.pt.transitSchedule.api.TransitRoute;
import org.matsim.pt.transitSchedule.api.TransitRouteStop;
import org.matsim.pt.transitSchedule.api.TransitStopFacility;
import org.matsim.vehicles.Vehicle;

public class MultiAVAgentDecorator implements MobsimDriverPassengerAgent, PlanAgent {
	private final MobsimDriverPassengerAgent driverPassengerDelegate;
	private final PlanAgent planDelegate;
	private boolean shouldPretendAV = false;

	public MultiAVAgentDecorator(MobsimAgent delegate) {
		this.driverPassengerDelegate = (MobsimDriverPassengerAgent) delegate;
		this.planDelegate = (PlanAgent) delegate;
	}

	public Id<Person> getId() {
		return driverPassengerDelegate.getId();
	}

	public Id<Link> getCurrentLinkId() {
		return driverPassengerDelegate.getCurrentLinkId();
	}

	public Id<Link> getDestinationLinkId() {
		return driverPassengerDelegate.getDestinationLinkId();
	}

	public State getState() {
		return driverPassengerDelegate.getState();
	}

	public double getActivityEndTime() {
		return driverPassengerDelegate.getActivityEndTime();
	}

	public void pretendAV() {
		shouldPretendAV = true;
	}

	public String getMode() {
		if (shouldPretendAV) {
			return "av";
		}

		return driverPassengerDelegate.getMode();
	}

	public void endActivityAndComputeNextState(double now) {
		driverPassengerDelegate.endActivityAndComputeNextState(now);
	}

	public void endLegAndComputeNextState(double now) {
		driverPassengerDelegate.endLegAndComputeNextState(now);
	}

	public void setStateToAbort(double now) {
		driverPassengerDelegate.setStateToAbort(now);
	}

	public Double getExpectedTravelTime() {
		return driverPassengerDelegate.getExpectedTravelTime();
	}

	public Double getExpectedTravelDistance() {
		return driverPassengerDelegate.getExpectedTravelDistance();
	}

	public void notifyArrivalOnLinkByNonNetworkMode(Id<Link> linkId) {
		shouldPretendAV = false;
		driverPassengerDelegate.notifyArrivalOnLinkByNonNetworkMode(linkId);
	}

	public Facility<? extends Facility<?>> getCurrentFacility() {
		return driverPassengerDelegate.getCurrentFacility();
	}

	public Facility<? extends Facility<?>> getDestinationFacility() {
		return driverPassengerDelegate.getDestinationFacility();
	}

	@Override
	public void setVehicle(MobsimVehicle veh) {
		driverPassengerDelegate.setVehicle(veh);
	}

	@Override
	public MobsimVehicle getVehicle() {
		return driverPassengerDelegate.getVehicle();
	}

	@Override
	public Id<Vehicle> getPlannedVehicleId() {
		return driverPassengerDelegate.getPlannedVehicleId();
	}

	@Override
	public PlanElement getCurrentPlanElement() {
		return planDelegate.getCurrentPlanElement();
	}

	@Override
	public PlanElement getNextPlanElement() {
		return planDelegate.getNextPlanElement();
	}

	@Override
	public PlanElement getPreviousPlanElement() {
		return planDelegate.getPreviousPlanElement();
	}

	@Override
	public Plan getCurrentPlan() {
		return planDelegate.getCurrentPlan();
	}

	@Override
	public boolean getEnterTransitRoute(TransitLine line, TransitRoute transitRoute, List<TransitRouteStop> stopsToCome,
			TransitVehicle transitVehicle) {
		return driverPassengerDelegate.getEnterTransitRoute(line, transitRoute, stopsToCome, transitVehicle);
	}

	@Override
	public boolean getExitAtStop(TransitStopFacility stop) {
		return driverPassengerDelegate.getExitAtStop(stop);
	}

	@Override
	public Id<TransitStopFacility> getDesiredAccessStopId() {
		return driverPassengerDelegate.getDesiredAccessStopId();
	}

	@Override
	public Id<TransitStopFacility> getDesiredDestinationStopId() {
		return driverPassengerDelegate.getDesiredAccessStopId();
	}

	@Override
	public double getWeight() {
		return driverPassengerDelegate.getWeight();
	}

	@Override
	public Id<Link> chooseNextLinkId() {
		return driverPassengerDelegate.chooseNextLinkId();
	}

	@Override
	public void notifyMoveOverNode(Id<Link> newLinkId) {
		driverPassengerDelegate.notifyMoveOverNode(newLinkId);
	}

	@Override
	public boolean isWantingToArriveOnCurrentLink() {
		return driverPassengerDelegate.isWantingToArriveOnCurrentLink();
	}
}
