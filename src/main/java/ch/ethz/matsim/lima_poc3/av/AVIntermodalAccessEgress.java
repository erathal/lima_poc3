package ch.ethz.matsim.lima_poc3.av;

import java.util.List;

import org.matsim.api.core.v01.population.Leg;
import org.matsim.api.core.v01.population.PlanElement;
import org.matsim.api.core.v01.population.Route;
import org.matsim.core.utils.misc.Time;

import ch.ethz.matsim.lima_poc3.av.routing.ExtendedAVRoute;
import ch.sbb.matsim.routing.pt.raptor.RaptorIntermodalAccessEgress;
import ch.sbb.matsim.routing.pt.raptor.RaptorParameters;

public class AVIntermodalAccessEgress implements RaptorIntermodalAccessEgress {
	@Override
	public RIntermodalAccessEgress calcIntermodalAccessEgress(List<? extends PlanElement> legs,
			RaptorParameters params) {
		// This is mainly a copy & paste from DefaultRaptorIntermodalAccessEgress

		double disutility = 0.0;
		double tTime = 0.0;

		for (PlanElement pe : legs) {
			if (pe instanceof Leg) {
				String mode = ((Leg) pe).getMode();
				Route route = ((Leg) pe).getRoute();

				double travelTime = ((Leg) pe).getTravelTime();

				if (Time.getUndefinedTime() != travelTime) {
					tTime += travelTime;
					disutility += travelTime * -params.getMarginalUtilityOfTravelTime_utl_s(mode);
				} else {
					throw new IllegalStateException();
				}

				// Added for AVs

				if (route instanceof ExtendedAVRoute) {
					double waitingTime = ((ExtendedAVRoute) route).getPickupTime();

					if (Time.getUndefinedTime() != waitingTime) {
						disutility += waitingTime * -params.getMarginalUtilityOfWaitingPt_utl_s();
					} else {
						throw new IllegalStateException();
					}
				}
			}
		}

		return new RIntermodalAccessEgress(legs, disutility, tTime);
	}
}
