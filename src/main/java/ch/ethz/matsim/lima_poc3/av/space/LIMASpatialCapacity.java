package ch.ethz.matsim.lima_poc3.av.space;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;

public class LIMASpatialCapacity implements SpatialCapacity {
	private final List<Double> thresholds;
	private final Map<Id<Link>, List<Long>> capacity;

	public LIMASpatialCapacity(List<Double> thresholds, Map<Id<Link>, List<Long>> capacity) {
		this.thresholds = thresholds;
		this.capacity = capacity;
	}

	@Override
	public long getSpatialCapacity(Link link, double time) {
		List<Long> capacities = capacity.get(link.getId());

		if (capacities == null) {
			return 0;
		} else {
			int index = 0;

			while (time < thresholds.get(index)) {
				index++;
			}

			return capacities.get(index);
		}
	}

	static public LIMASpatialCapacity load(URL url) {
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));

			List<String> header = Arrays.asList(reader.readLine().split(";"));
			List<Double> thresholds = new ArrayList<>(header.size() - 1);

			for (int i = 1; i < header.size(); i++) {
				thresholds.add(Double.parseDouble(header.get(i)));
			}

			String raw = null;
			List<String> row = null;

			Map<Id<Link>, List<Long>> capacities = new HashMap<>();

			while ((raw = reader.readLine()) != null) {
				row = Arrays.asList(raw.split(";"));

				Id<Link> linkId = Id.createLinkId(row.get(0));
				List<Long> capacity = new ArrayList<>(header.size() - 1);

				for (int i = 1; i < header.size(); i++) {
					capacity.add(Long.parseLong(row.get(i)));
				}

				capacities.put(linkId, capacity);
			}

			reader.close();

			return new LIMASpatialCapacity(thresholds, capacities);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
