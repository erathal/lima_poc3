package ch.ethz.matsim.lima_poc3.av.simulation;

import java.util.Collection;
import java.util.Collections;

import org.matsim.core.config.Config;
import org.matsim.core.mobsim.qsim.AbstractQSimPlugin;
import org.matsim.core.mobsim.qsim.interfaces.DepartureHandler;

import com.google.inject.AbstractModule;
import com.google.inject.Module;

public class MultiAVQSimPlugin extends AbstractQSimPlugin {
	public MultiAVQSimPlugin(Config config) {
		super(config);
	}

	public Collection<Class<? extends DepartureHandler>> departureHandlers() {
		return Collections.singleton(MultiAVDepartureHandler.class);
	}

	public Collection<? extends Module> modules() {
		return Collections.singleton(new AbstractModule() {
			@Override
			protected void configure() {
				bind(MultiAVDepartureHandler.class);
			}
		});
	}
}
