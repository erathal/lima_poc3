package ch.ethz.matsim.lima_poc3.av.scoring;

import org.matsim.api.core.v01.events.Event;
import org.matsim.api.core.v01.events.PersonArrivalEvent;
import org.matsim.api.core.v01.events.PersonDepartureEvent;
import org.matsim.api.core.v01.events.PersonEntersVehicleEvent;
import org.matsim.core.scoring.SumScoringFunction.ArbitraryEventScoring;
import org.matsim.core.scoring.functions.ScoringParameters;

import ch.ethz.matsim.av.schedule.AVTransitEvent;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;

public class MultiAVScoring implements ArbitraryEventScoring {
	private final ScoringParameters params;
	private final LIMAScoringParameters limaParams;

	private String ongoingMode = null;
	private double departureTime = 0.0;

	private double score = 0.0;

	public MultiAVScoring(ScoringParameters params, LIMAScoringParameters limaParams) {
		this.params = params;
		this.limaParams = limaParams;
	}

	public void handleDeparture(PersonDepartureEvent departureEvent) {
		if (departureEvent.getLegMode().startsWith("av")) {
			if (ongoingMode != null) {
				throw new IllegalStateException();
			}

			ongoingMode = departureEvent.getLegMode();
			departureTime = departureEvent.getTime();
		}
	}

	public void handleEntersVehicle(PersonEntersVehicleEvent entersVehicleEvent) {
		if (ongoingMode != null) {
			double marginalUtilityOfAvWaitingTime = 0;

			switch (ongoingMode) {
			case AVDirectRoutingModule.AV_DIRECT_MODE:
				marginalUtilityOfAvWaitingTime = limaParams.marginalUtilityOfDirectWaitingTime_s;
				break;
			case AVFeederRoutingModule.AV_FEEDER_MODE:
				marginalUtilityOfAvWaitingTime = limaParams.marginalUtilityOfFeederWaitingTime_s;
				break;
			default:
				throw new IllegalStateException();
			}

			double waitingTime = entersVehicleEvent.getTime() - departureTime;
			score += waitingTime * marginalUtilityOfAvWaitingTime;
		}
	}

	public void handleTransit(AVTransitEvent transitEvent) {
		if (ongoingMode == null) {
			throw new IllegalStateException();
		} else {
			score += transitEvent.getDistance() * params.modeParams.get(ongoingMode).marginalUtilityOfDistance_m;
			score += transitEvent.getDistance() * params.modeParams.get(ongoingMode).monetaryDistanceCostRate
					* params.marginalUtilityOfMoney;
		}
	}

	public void handleArrival(PersonArrivalEvent arrivalEvent) {
		if (ongoingMode != null) {
			if (ongoingMode.equals(arrivalEvent.getLegMode())) {
				ongoingMode = null;
			} else {
				throw new IllegalStateException();
			}
		}
	}

	@Override
	public void finish() {
		if (ongoingMode != null) {
			score = params.abortedPlanScore;
		}
	}

	@Override
	public double getScore() {
		return score;
	}

	@Override
	public void handleEvent(Event event) {
		if (event instanceof PersonDepartureEvent) {
			handleDeparture((PersonDepartureEvent) event);
		}

		if (event instanceof PersonEntersVehicleEvent) {
			handleEntersVehicle((PersonEntersVehicleEvent) event);
		}

		if (event instanceof AVTransitEvent) {
			handleTransit((AVTransitEvent) event);
		}

		if (event instanceof PersonArrivalEvent) {
			handleArrival((PersonArrivalEvent) event);
		}
	}

}
