package ch.ethz.matsim.lima_poc3.av.space;

import org.matsim.api.core.v01.network.Link;

public class ConstantSpatialCapacity implements SpatialCapacity {
	private final long capacity;

	public ConstantSpatialCapacity(long capacity) {
		this.capacity = capacity;
	}

	@Override
	public long getSpatialCapacity(Link link, double time) {
		return capacity;
	}
}
