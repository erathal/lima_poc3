package ch.ethz.matsim.lima_poc3.av.scoring;

import java.util.HashMap;
import java.util.Map;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.population.Person;
import org.matsim.utils.objectattributes.ObjectAttributes;

import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.ScoringParameterSet;

public class LIMAScoringParameterForPerson {
	private final String subpopulationAttributeName;
	private final ObjectAttributes personAttributes;
	private final LimaAvConfigGroup limaConfig;
	private final Map<String, LIMAScoringParameters> scoringParameters = new HashMap<>();

	public LIMAScoringParameterForPerson(LimaAvConfigGroup limaConfig, Scenario scenario) {
		this.subpopulationAttributeName = scenario.getConfig().plans().getSubpopulationAttributeName();
		this.personAttributes = scenario.getPopulation().getPersonAttributes();
		this.limaConfig = limaConfig;
	}

	public LIMAScoringParameters getScoringParameters(Person person) {
		String subpopulation = (String) personAttributes.getAttribute(person.getId().toString(),
				subpopulationAttributeName);

		if (!scoringParameters.containsKey(subpopulation)) {
			ScoringParameterSet scoringConfig = limaConfig.getScoringParametersForSubpopulation(subpopulation);

			double marginalUtilityOfDirectWaitingTime_s = scoringConfig.getMarginalUtilityOfDirectWaitingTime()
					/ 3600.0;
			double marginalUtilityOfFeederWaitingTime_s = scoringConfig.getMarginalUtilityOfFeederWaitingTime()
					/ 3600.0;

			scoringParameters.put(subpopulation, new LIMAScoringParameters(marginalUtilityOfDirectWaitingTime_s,
					marginalUtilityOfFeederWaitingTime_s));
		}

		return scoringParameters.get(subpopulation);
	}
}
