package ch.ethz.matsim.lima_poc3.av.space;

import org.matsim.api.core.v01.network.Link;

public class AttributeSpatialCapacity implements SpatialCapacity {
	private final String attribute;

	public AttributeSpatialCapacity(String attribute) {
		this.attribute = attribute;
	}

	@Override
	public long getSpatialCapacity(Link link, double time) {
		Long capacity = (Long) link.getAttributes().getAttribute(attribute);

		if (capacity != null) {
			return capacity;
		} else {
			return 0;
		}
	}
}
