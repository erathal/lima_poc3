package ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking;

import java.util.concurrent.Future;

import org.matsim.api.core.v01.network.Link;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;

import ch.ethz.matsim.av.data.AVVehicle;

public class ParkingAppendTask {
	public final AVVehicle vehicle;

	public final Future<Path> pathFuture;

	public final Link originLink;
	public final Link destinationLink;

	public ParkingAppendTask(AVVehicle vehicle, Link originLink, Link destinationLink, Future<Path> pathFuture) {
		this.originLink = originLink;
		this.destinationLink = destinationLink;
		this.vehicle = vehicle;
		this.pathFuture = pathFuture;
	}
}
