package ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking;

import org.matsim.api.core.v01.network.Link;

public class ParkingLinkWrapper {
	final Link link;
	long numberOfVehicles;

	public ParkingLinkWrapper(Link link, long numberOfVehicles) {
		this.numberOfVehicles = numberOfVehicles;
		this.link = link;
	}
}