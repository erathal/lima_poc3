package ch.ethz.matsim.lima_poc3.av.routing.spatial;

import org.matsim.facilities.Facility;

public interface AVInteractionFacilityFinder {
	Facility<?> findPickupFacility(Facility<?> originFacility, double time);

	Facility<?> findDropoffFacility(Facility<?> destinationFacility, double time);
}
