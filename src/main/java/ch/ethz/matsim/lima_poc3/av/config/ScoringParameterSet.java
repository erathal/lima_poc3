package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class ScoringParameterSet extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "scoring";

	public final static String SUBPOPUALTION = "subpopulation";

	public final static String MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME = "marginalUtilityOfFeederWaitingTime";
	public final static String MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME = "marginalUtilityOfDirectWaitingTime";

	private String subpopulation = null;

	private double marignalUtilityOfFeederWaitingTime;
	private double marginalUtilityOfDirectWaitingTime;

	public ScoringParameterSet() {
		super(GROUP_NAME);
	}

	@StringSetter(SUBPOPUALTION)
	public void setSubpopulation(String subpopulation) {
		this.subpopulation = subpopulation;
	}

	@StringGetter(SUBPOPUALTION)
	public String getSubpopulation() {
		return subpopulation;
	}

	@StringSetter(MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME)
	public void setMarginalUtilityOfDirectWaitingTime(double marginalUtilityOfDirectWaitingTime) {
		this.marginalUtilityOfDirectWaitingTime = marginalUtilityOfDirectWaitingTime;
	}

	@StringGetter(MARGINAL_UTILITY_OF_DIRECT_WAITING_TIME)
	public double getMarginalUtilityOfDirectWaitingTime() {
		return marginalUtilityOfDirectWaitingTime;
	}

	@StringSetter(MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME)
	public void setMarginalUtilityOfFeederWaitingTime(double marignalUtilityOfFeederWaitingTime) {
		this.marignalUtilityOfFeederWaitingTime = marignalUtilityOfFeederWaitingTime;
	}

	@StringGetter(MARGINAL_UTILITY_OF_FEEDER_WAITING_TIME)
	public double getMarginalUtilityOfFeederWaitingTime() {
		return marignalUtilityOfFeederWaitingTime;
	}
}
