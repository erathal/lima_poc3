package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class ConstantInteractionTimeParameterSet extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "InteractionTime:Constant";

	public final static String PICKUP_TIME = "pickupTime_s";
	public final static String DROPOFF_TIME = "dropoffTime_s";

	private double pickupTime = 300.0;
	private double dropoffTime = 300.0;

	public ConstantInteractionTimeParameterSet() {
		super(GROUP_NAME);
	}

	@StringGetter(PICKUP_TIME)
	public double getPickupTime() {
		return pickupTime;
	}

	@StringSetter(PICKUP_TIME)
	public void setPickupTime(double pickupTime) {
		this.pickupTime = pickupTime;
	}

	@StringGetter(DROPOFF_TIME)
	public double getDropoffTime() {
		return dropoffTime;
	}

	@StringSetter(DROPOFF_TIME)
	public void setDropoffTime(double dropoffTime) {
		this.dropoffTime = dropoffTime;
	}
}
