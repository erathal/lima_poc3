package ch.ethz.matsim.lima_poc3.av.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaSpatialCapacityParameterSet extends ReflectiveConfigGroup {
	public static final String GROUP_NAME = "SpatialCapacity:Lima";

	public static final String PARKING_CAPACITY_PATH = "parkingCapacityPath";
	public static final String PICKUP_DROPOFF_CAPACITY_PATH = "pickupDropoffCapacityPath";

	private String parkingCapacityPath = null;
	private String pickupDropoffCapacityPath = null;

	public LimaSpatialCapacityParameterSet() {
		super(GROUP_NAME);
	}

	@StringGetter(PARKING_CAPACITY_PATH)
	public String getParkingCapacityPath() {
		return parkingCapacityPath;
	}

	@StringSetter(PARKING_CAPACITY_PATH)
	public void setParkingCapacityPath(String parkingCapacityPath) {
		this.parkingCapacityPath = parkingCapacityPath;
	}

	@StringGetter(PICKUP_DROPOFF_CAPACITY_PATH)
	public String getPickupDropoffCapacityPath() {
		return pickupDropoffCapacityPath;
	}

	@StringSetter(PICKUP_DROPOFF_CAPACITY_PATH)
	public void setPickupDropoffCapacityPath(String pickupDropoffCapacityPath) {
		this.pickupDropoffCapacityPath = pickupDropoffCapacityPath;
	}
}
