package ch.ethz.matsim.lima_poc3.av.scoring;

public class LIMAScoringParameters {
	public final double marginalUtilityOfDirectWaitingTime_s;
	public final double marginalUtilityOfFeederWaitingTime_s;

	public LIMAScoringParameters(double marginalUtilityOfDirectWaitingTime_s,
			double marginalUtilityOfFeederWaitingTime_s) {
		this.marginalUtilityOfDirectWaitingTime_s = marginalUtilityOfDirectWaitingTime_s;
		this.marginalUtilityOfFeederWaitingTime_s = marginalUtilityOfFeederWaitingTime_s;
	}
}
