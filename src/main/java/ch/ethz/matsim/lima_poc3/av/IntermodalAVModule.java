package ch.ethz.matsim.lima_poc3.av;

import java.net.URL;
import java.util.Map;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.router.DvrpRoutingNetworkProvider;
import org.matsim.core.config.ConfigGroup;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.OutputDirectoryHierarchy;
import org.matsim.core.scoring.ScoringFunctionFactory;

import com.google.inject.Key;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.multibindings.MapBinder;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.config.AVConfig;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.av.analysis.LIMAWaitingTimeListener;
import ch.ethz.matsim.lima_poc3.av.config.ConstantInteractionTimeParameterSet;
import ch.ethz.matsim.lima_poc3.av.config.ConstantSpatialCapacityParameterSet;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup.InteractionTimeEstimator;
import ch.ethz.matsim.lima_poc3.av.config.LimaInteractionTimeParameterSet;
import ch.ethz.matsim.lima_poc3.av.config.LimaSpatialCapacityParameterSet;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.AVFeederRoutingModule;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.AVInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.AccessEgressLinkData;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.CapacityAwareRadiusInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.IntermodalInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionFacilityFinder;
import ch.ethz.matsim.lima_poc3.av.routing.spatial.NearestInteractionLinkData;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.AVInteractionTime;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.ConstantAVInteractionTime;
import ch.ethz.matsim.lima_poc3.av.routing.temporal.estimation.LIMAInteractionTime;
import ch.ethz.matsim.lima_poc3.av.scoring.LIMAScoringParameterForPerson;
import ch.ethz.matsim.lima_poc3.av.scoring.MultiAVScoringFunctionFactory;
import ch.ethz.matsim.lima_poc3.av.space.ConstantSpatialCapacity;
import ch.ethz.matsim.lima_poc3.av.space.LIMASpatialCapacity;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;
import ch.sbb.matsim.routing.pt.raptor.RaptorIntermodalAccessEgress;

public class IntermodalAVModule extends AbstractModule {
	@Override
	public void install() {
		bind(Key.get(Network.class, Names.named(DvrpRoutingNetworkProvider.DVRP_ROUTING)))
				.toProvider(DvrpRoutingNetworkProvider.class).asEagerSingleton();

		addRoutingModuleBinding(AVFeederRoutingModule.AV_FEEDER_MODE).to(AVFeederRoutingModule.class);
		addRoutingModuleBinding(AVDirectRoutingModule.AV_DIRECT_MODE).to(AVDirectRoutingModule.class);

		bind(ScoringFunctionFactory.class).to(MultiAVScoringFunctionFactory.class);
		bind(RaptorIntermodalAccessEgress.class).to(AVIntermodalAccessEgress.class);

		MapBinder<String, AVInteractionFacilityFinder> interactionBinder = MapBinder.newMapBinder(binder(),
				String.class, AVInteractionFacilityFinder.class);
		interactionBinder.addBinding("nearest").to(NearestInteractionFacilityFinder.class);
		interactionBinder.addBinding("radius").to(CapacityAwareRadiusInteractionFacilityFinder.class);

		bind(IntermodalInteractionFacilityFinder.class);

		bind(NearestInteractionLinkData.class).asEagerSingleton();
		bind(AccessEgressLinkData.class).asEagerSingleton();

		LimaAvConfigGroup config = (LimaAvConfigGroup) getConfig().getModules().get(LimaAvConfigGroup.GROUP_NAME);

		if (config.getWaitingTimeType().equals(InteractionTimeEstimator.Constant)) {
			bind(AVInteractionTime.class).to(ConstantAVInteractionTime.class);
		} else if (config.getWaitingTimeType().equals(InteractionTimeEstimator.Lima)) {
			bind(AVInteractionTime.class).to(LIMAInteractionTime.class);
			addEventHandlerBinding().to(LIMAInteractionTime.class);
			addControlerListenerBinding().to(LIMAWaitingTimeListener.class);
		}

		if (config.getSpatialCapacity().equals(LimaAvConfigGroup.SpatialCapacity.Constant)) {
			ConstantSpatialCapacityParameterSet scConfig = config.getConstantSpatialCapacityConfig();
			bind(Key.get(SpatialCapacity.class, Names.named("parking")))
					.toInstance(new ConstantSpatialCapacity(scConfig.getParkingCapacity()));
			bind(Key.get(SpatialCapacity.class, Names.named("pickupDropoff")))
					.toInstance(new ConstantSpatialCapacity(scConfig.getPickupDropoffCapacity()));
		} else {
			LimaSpatialCapacityParameterSet scConfig = config.getLimaSpatialCapacityConfig();

			if (scConfig.getParkingCapacityPath() == null) {
				throw new IllegalStateException("No input path given for parking capacity.");
			}

			if (scConfig.getPickupDropoffCapacityPath() == null) {
				throw new IllegalStateException("No input path given for pickup/dropoff capacity.");
			}

			URL parkingUrl = ConfigGroup.getInputFileURL(getConfig().getContext(), scConfig.getParkingCapacityPath());
			URL pickupDropoffUrl = ConfigGroup.getInputFileURL(getConfig().getContext(),
					scConfig.getPickupDropoffCapacityPath());

			LIMASpatialCapacity parkingCapacity = LIMASpatialCapacity.load(parkingUrl);
			LIMASpatialCapacity pickupDropoffCapacity = LIMASpatialCapacity.load(pickupDropoffUrl);

			bind(Key.get(SpatialCapacity.class, Names.named("parking"))).toInstance(parkingCapacity);
			bind(Key.get(SpatialCapacity.class, Names.named("pickupDropoff"))).toInstance(pickupDropoffCapacity);
		}
	}

	@Provides
	public AVInteractionFacilityFinder provideAVInteractionFacilityFinder(LimaAvConfigGroup limaConfig,
			Map<String, Provider<AVInteractionFacilityFinder>> providers) {
		switch (limaConfig.getInteractionPointFinder()) {
		case CapacityAwareWithinRadius:
			return providers.get("radius").get();
		case Nearest:
			return providers.get("nearest").get();
		default:
			throw new IllegalStateException();
		}
	}

	@Provides
	@Singleton
	public LimaInteractionTimeParameterSet provideInteractionTimeParameterSet(LimaAvConfigGroup config) {
		return config.getLimaInteractionTimeConfig();
	}

	@Provides
	@Singleton
	public ConstantInteractionTimeParameterSet provideConstantInteractionTimeParameterSet(LimaAvConfigGroup config) {
		return config.getConstantInteractionTimeConfig();
	}

	@Provides
	@Singleton
	public LIMAWaitingTimeListener provideLimaWaitingTimeListener(LimaInteractionTimeParameterSet config,
			LIMAInteractionTime interactionTime, Network network, OutputDirectoryHierarchy outputDirectoryHierarchy) {
		return new LIMAWaitingTimeListener(config, interactionTime, network, outputDirectoryHierarchy);
	}

	@Provides
	@Singleton
	public LIMAScoringParameterForPerson provideScoringParametersForLIMA(LimaAvConfigGroup limaConfig,
			Scenario scenario) {
		return new LIMAScoringParameterForPerson(limaConfig, scenario);
	}

	@Provides
	@Singleton
	public ConstantAVInteractionTime provideAVInteractionTime(AVConfig config,
			ConstantInteractionTimeParameterSet itConfig) {
		double fixedPickupTime = config.getTimingParameters().getPickupDurationPerStop();
		double fixedDropoffTime = config.getTimingParameters().getDropoffDurationPerStop();
		double pickupTime = itConfig.getPickupTime();
		double dropoffTime = itConfig.getDropoffTime();

		return new ConstantAVInteractionTime(pickupTime, dropoffTime, fixedPickupTime, fixedDropoffTime);
	}

	@Provides
	@Singleton
	public LIMAInteractionTime provideLIMAInteractionTime(AVConfig config, @Named(AVModule.AV_MODE) Network network,
			LimaInteractionTimeParameterSet itConfig) {
		double fixedPickupTime = config.getTimingParameters().getPickupDurationPerStop();
		double fixedDropoffTime = config.getTimingParameters().getDropoffDurationPerStop();

		double estimationStartTime = itConfig.getEstimationStartTime();
		double estimationEndTime = itConfig.getEstimationEndTime();
		double estimationInterval = itConfig.getEstimationInterval();
		int horizon = itConfig.getHorizon();
		double defaultWaitingTime = itConfig.getDefaultWaitingTime();

		return new LIMAInteractionTime(estimationStartTime, estimationEndTime, estimationInterval, horizon,
				defaultWaitingTime, fixedPickupTime, fixedDropoffTime, network, itConfig.getLinkAttribute());
	}
}
