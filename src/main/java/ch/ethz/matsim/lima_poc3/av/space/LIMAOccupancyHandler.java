package ch.ethz.matsim.lima_poc3.av.space;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.vrpagent.VrpAgentLogic;
import org.matsim.contrib.dynagent.DynAgent;
import org.matsim.core.api.experimental.events.EventsManager;
import org.matsim.core.mobsim.framework.events.MobsimAfterSimStepEvent;
import org.matsim.core.mobsim.framework.events.MobsimBeforeSimStepEvent;
import org.matsim.core.mobsim.framework.listeners.MobsimAfterSimStepListener;
import org.matsim.core.mobsim.framework.listeners.MobsimBeforeSimStepListener;
import org.matsim.core.mobsim.qsim.qnetsimengine.QVehicle;

import ch.ethz.matsim.lima_poc3.av.components.LIMAVehicle;
import ch.ethz.matsim.lima_poc3.av.components.LIMAVehicle.Intention;

public class LIMAOccupancyHandler
		implements VehicleInteractionHandler, MobsimBeforeSimStepListener, MobsimAfterSimStepListener {
	private final LinkOccupancyHandler parkingHandler;
	private final LinkOccupancyHandler pickupDropoffHandler;

	private final Set<QVehicle> parkingVehicles = new HashSet<>();
	private boolean isBeforeStart = true;

	public LIMAOccupancyHandler(SpatialCapacity parkingCapacity, SpatialCapacity pickupDropoffCapacity, Network network,
			EventsManager eventsManager) {
		VehicleInteractionHandler.INSTANCE.setDelegate(this);
		this.parkingHandler = new LinkOccupancyHandler("parking", parkingCapacity, network, eventsManager);
		this.pickupDropoffHandler = new LinkOccupancyHandler("pickupDropoff", pickupDropoffCapacity, network,
				eventsManager);
	}

	@Override
	public void registerParkedVehicle(QVehicle vehicle, Link link) {
		if (isBeforeStart && vehicle.getId().toString().startsWith("av")) {
			parkingVehicles.add(vehicle);
			parkingHandler.registerParkedVehicle(vehicle, link);
		}
	}

	private static Method getVehicleMethod;

	static {
		try {
			getVehicleMethod = VrpAgentLogic.class.getDeclaredMethod("getVehicle");
			getVehicleMethod.setAccessible(true);
		} catch (NoSuchMethodException | SecurityException e) {
			throw new RuntimeException(e);
		}
	}

	private LIMAVehicle getLIMAVehicle(QVehicle vehicle) {
		try {
			VrpAgentLogic logic = ((VrpAgentLogic) ((DynAgent) vehicle.getDriver()).getAgentLogic());
			return (LIMAVehicle) getVehicleMethod.invoke(logic);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public boolean handleArrival(QVehicle vehicle, Link link) {
		synchronized (this) {
			if (vehicle.getId().toString().startsWith("av")) {
				boolean wantsToPark = getLIMAVehicle(vehicle).getIntention().equals(Intention.Parking);

				if (wantsToPark) {
					if (parkingHandler.handleArrival(vehicle, link)) {
						parkingVehicles.add(vehicle);
						return true;
					} else {
						return false;
					}
				} else {
					return pickupDropoffHandler.handleArrival(vehicle, link);
				}
			}

			return true;
		}
	}

	@Override
	public void handleDeparture(QVehicle vehicle, Link link) {
		synchronized (this) {
			if (vehicle.getId().toString().startsWith("av")) {
				if (parkingVehicles.remove(vehicle)) {
					parkingHandler.handleDeparture(vehicle, link);
				} else {
					pickupDropoffHandler.handleDeparture(vehicle, link);
				}
			}
		}
	}

	@Override
	public void notifyMobsimBeforeSimStep(@SuppressWarnings("rawtypes") MobsimBeforeSimStepEvent e) {
		isBeforeStart = false;
		parkingHandler.notifyMobsimBeforeSimStep(e);
		pickupDropoffHandler.notifyMobsimBeforeSimStep(e);
	}

	@Override
	public void notifyMobsimAfterSimStep(@SuppressWarnings("rawtypes") MobsimAfterSimStepEvent e) {
		parkingHandler.notifyMobsimAfterSimStep(e);
		pickupDropoffHandler.notifyMobsimAfterSimStep(e);
	}
}
