package ch.ethz.matsim.lima_poc3.av.amodeus;

import org.matsim.core.utils.geometry.CoordinateTransformation;
import org.matsim.core.utils.geometry.transformations.IdentityTransformation;

import ch.ethz.idsc.amodeus.data.ReferenceFrame;

public class SandboxReferenceFrame implements ReferenceFrame {
	@Override
	public CoordinateTransformation coords_fromWGS84() {
		return new IdentityTransformation();
	}

	@Override
	public CoordinateTransformation coords_toWGS84() {
		return new IdentityTransformation();
	}
}
