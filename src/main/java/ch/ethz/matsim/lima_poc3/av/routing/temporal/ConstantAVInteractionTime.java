package ch.ethz.matsim.lima_poc3.av.routing.temporal;

import org.matsim.facilities.Facility;

public class ConstantAVInteractionTime implements AVInteractionTime {
	private final double pickupTime;
	private final double fixedPickupTime;
	private final double dropoffTime;

	public ConstantAVInteractionTime(double pickupTime, double dropoffTime, double fixedPickupTime,
			double fixedDropoffTime) {
		this.pickupTime = pickupTime;
		this.fixedPickupTime = fixedPickupTime;
		this.dropoffTime = dropoffTime + fixedDropoffTime;
	}

	@Override
	public double getDropoffTime(Facility<?> facility, double time) {
		return dropoffTime;
	}

	@Override
	public double getPickupTime(Facility<?> facility, double time, double approachTime) {
		return Math.max(0, pickupTime - approachTime) + fixedPickupTime;
	}
}
