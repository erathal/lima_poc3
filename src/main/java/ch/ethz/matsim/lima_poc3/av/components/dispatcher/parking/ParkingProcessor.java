package ch.ethz.matsim.lima_poc3.av.components.dispatcher.parking;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import org.matsim.api.core.v01.network.Link;
import org.matsim.api.core.v01.network.Network;
import org.matsim.contrib.dvrp.path.VrpPathWithTravelData;
import org.matsim.contrib.dvrp.path.VrpPaths;
import org.matsim.contrib.dvrp.schedule.Schedule;
import org.matsim.contrib.dvrp.schedule.Schedules;
import org.matsim.contrib.dvrp.schedule.Task;
import org.matsim.core.network.NetworkUtils;
import org.matsim.core.router.util.LeastCostPathCalculator.Path;
import org.matsim.core.router.util.TravelTime;
import org.matsim.core.utils.collections.QuadTree;

import ch.ethz.matsim.av.data.AVVehicle;
import ch.ethz.matsim.av.plcpc.ParallelLeastCostPathCalculator;
import ch.ethz.matsim.av.schedule.AVDriveTask;
import ch.ethz.matsim.av.schedule.AVStayTask;
import ch.ethz.matsim.lima_poc3.av.space.SpatialCapacity;

public class ParkingProcessor {
	private final SpatialCapacity spatialCapacity;
	private final ParallelLeastCostPathCalculator router;
	private final TravelTime travelTime;

	private final Map<Link, ParkingLinkWrapper> parkingLinks = new HashMap<>();
	private final QuadTree<ParkingLinkWrapper> availableParkingIndex;
	private final Set<ParkingLinkWrapper> occupiedParking = new HashSet<>();

	private final Set<AVVehicle> parkingRequests = new HashSet<>();
	private final List<ParkingAppendTask> appendTasks = new LinkedList<>();

	public ParkingProcessor(Network network, SpatialCapacity spatialCapacity, ParallelLeastCostPathCalculator router,
			TravelTime travelTime) {
		double[] bounds = NetworkUtils.getBoundingBox(network.getNodes().values()); // minx, miny, maxx, maxy
		availableParkingIndex = new QuadTree<>(bounds[0], bounds[1], bounds[2], bounds[3]);

		this.spatialCapacity = spatialCapacity;
		this.router = router;
		this.travelTime = travelTime;

		for (Link link : network.getLinks().values()) {
			ParkingLinkWrapper wrapper = new ParkingLinkWrapper(link, 0);
			parkingLinks.put(link, wrapper);
			occupiedParking.add(wrapper);
		}
	}

	public void registerParking(Link link) {
		parkingLinks.get(link).numberOfVehicles++;
	}

	public void unregisterParking(Link link) {
		parkingLinks.get(link).numberOfVehicles--;
	}

	public void sendToPark(AVVehicle vehicle) {
		parkingRequests.add(vehicle);
	}

	public void update(double now) {
		// I) Process pending routing tasks
		try {
			processAppendTasks(now);
		} catch (InterruptedException | ExecutionException e) {
			throw new RuntimeException(e);
		}

		// II) Move occupied parkings to the available index if something got free
		Iterator<ParkingLinkWrapper> iterator = occupiedParking.iterator();

		while (iterator.hasNext()) {
			ParkingLinkWrapper parking = iterator.next();
			long capacity = spatialCapacity.getSpatialCapacity(parking.link, now);

			if (parking.numberOfVehicles < capacity) {
				iterator.remove();
				availableParkingIndex.put(parking.link.getCoord().getX(), parking.link.getCoord().getY(), parking);
			}
		}

		// III) Go through the parking requests
		for (AVVehicle vehicle : parkingRequests) {
			if (availableParkingIndex.size() == 0) {
				throw new IllegalStateException("Tried to find a parking spot, but whole system is occupied.");
			}

			Link originLink = ((AVStayTask) vehicle.getSchedule().getCurrentTask()).getLink();

			ParkingLinkWrapper parking = availableParkingIndex.getClosest(originLink.getCoord().getX(),
					originLink.getCoord().getY());
			parking.numberOfVehicles++;

			long capacity = spatialCapacity.getSpatialCapacity(parking.link, now);

			// If we just filled up the parking space, remove it from index and add it to
			// "occupied"
			if (parking.numberOfVehicles >= capacity) {
				availableParkingIndex.remove(parking.link.getCoord().getX(), parking.link.getCoord().getY(), parking);
				occupiedParking.add(parking);
			}

			// Add routing task to be processed next time step
			Future<Path> pathFuture = router.calcLeastCostPath(originLink.getToNode(), parking.link.getFromNode(), now,
					null, null);
			appendTasks.add(new ParkingAppendTask(vehicle, originLink, parking.link, pathFuture));
		}

		parkingRequests.clear();
	}

	private void processAppendTasks(double now) throws InterruptedException, ExecutionException {
		for (ParkingAppendTask task : appendTasks) {
			AVVehicle vehicle = task.vehicle;

			Schedule schedule = vehicle.getSchedule();
			AVStayTask stayTask = (AVStayTask) Schedules.getLastTask(schedule);

			double startTime = 0.0;
			double scheduleEndTime = schedule.getEndTime();

			if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
				startTime = now;
			} else {
				startTime = stayTask.getBeginTime();
			}

			Path path = task.pathFuture.get();

			VrpPathWithTravelData vrpPath = VrpPaths.createPath(task.originLink, task.destinationLink, startTime, path,
					travelTime);
			AVDriveTask driveTask = new AVDriveTask(vrpPath);

			if (stayTask.getStatus() == Task.TaskStatus.STARTED) {
				stayTask.setEndTime(startTime);
			} else {
				schedule.removeLastTask();
			}

			schedule.addTask(driveTask);

			if (driveTask.getEndTime() < scheduleEndTime) {
				schedule.addTask(new AVStayTask(driveTask.getEndTime(), scheduleEndTime, task.destinationLink));
			}
		}

		appendTasks.clear();
	}
}
