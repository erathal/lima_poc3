package ch.ethz.matsim.lima_poc3.av.routing.temporal.estimation;

public class MovingAverageIndexEstimator implements IndexEstimator {
	private final int numberOfItemIndices;
	private final int numberOfTimeIndices;
	private final double defaultValue;
	private final int horizon;

	private double[][][] cumulativeValues;
	private int[][][] observationCounts;

	private double[][] estimates;
	private int currentHorizonIndex = 0;

	public MovingAverageIndexEstimator(int numberOfItemIndices, int numberOfTimeIndices, int horizon,
			double defaultValue) {
		this.numberOfItemIndices = numberOfItemIndices;
		this.numberOfTimeIndices = numberOfTimeIndices;
		this.defaultValue = defaultValue;
		this.horizon = horizon;

		this.cumulativeValues = new double[horizon][numberOfItemIndices][numberOfTimeIndices];
		this.observationCounts = new int[horizon][numberOfItemIndices][numberOfTimeIndices];
		this.estimates = new double[numberOfItemIndices][numberOfTimeIndices];

		for (int i = 0; i < numberOfItemIndices; i++) {
			for (int t = 0; t < numberOfTimeIndices; t++) {
				estimates[i][t] = defaultValue;
			}
		}
	}

	@Override
	public void record(int itemIndex, int timeIndex, double value) {
		cumulativeValues[currentHorizonIndex][itemIndex][timeIndex] += value;
		observationCounts[currentHorizonIndex][itemIndex][timeIndex] += 1;
	}

	@Override
	public double estimate(int itemIndex, int timeIndex) {
		return estimates[itemIndex][timeIndex];
	}

	@Override
	public void finish() {
		for (int i = 0; i < numberOfItemIndices; i++) {
			for (int t = 0; t < numberOfTimeIndices; t++) {
				estimates[i][t] = 0.0;
				int count = 0;

				for (int h = 0; h < horizon; h++) {
					estimates[i][t] += cumulativeValues[h][i][t];
					count += observationCounts[h][i][t];
				}

				if (count == 0) {
					estimates[i][t] = defaultValue;
				} else {
					estimates[i][t] /= count;
				}
			}
		}

		currentHorizonIndex += 1;

		if (currentHorizonIndex == horizon) {
			currentHorizonIndex = 0;
		}
	}
}
