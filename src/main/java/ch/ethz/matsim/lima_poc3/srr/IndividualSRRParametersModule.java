package ch.ethz.matsim.lima_poc3.srr;

import org.matsim.core.controler.AbstractModule;

import ch.sbb.matsim.routing.pt.raptor.RaptorParametersForPerson;

public class IndividualSRRParametersModule extends AbstractModule {
	@Override
	public void install() {
		bind(RaptorParametersForPerson.class).to(IndividualRaptorParametersForPerson.class);
	}
}
