package ch.ethz.matsim.lima_poc3;

import java.util.Arrays;

import org.matsim.contrib.dvrp.trafficmonitoring.DvrpTravelTimeModule;
import org.matsim.core.config.Config;
import org.matsim.core.controler.AbstractModule;
import org.matsim.core.controler.Controler;

import ch.ethz.matsim.av.framework.AVConfigGroup;
import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceConfigurator;
import ch.ethz.matsim.discrete_mode_choice.modules.DiscreteModeChoiceModule;
import ch.ethz.matsim.discrete_mode_choice.modules.EstimatorModule;
import ch.ethz.matsim.discrete_mode_choice.modules.config.DiscreteModeChoiceConfigGroup;
import ch.ethz.matsim.discrete_mode_choice.modules.config.LinkAttributeConstraintConfigGroup;
import ch.ethz.matsim.lima_poc3.av.IntermodalAVModule;
import ch.ethz.matsim.lima_poc3.av.LimaAvModule;
import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.routing.AVDirectRoutingModule;
import ch.ethz.matsim.lima_poc3.dmc.LimaDiscreteModeChoiceModule;
import ch.ethz.matsim.lima_poc3.dmc.LimaModeChoiceConfigGroup;
import ch.ethz.matsim.lima_poc3.flow_efficiency.LimaFlowEfficiencyModule;
import ch.ethz.matsim.lima_poc3.flow_efficiency.config.LimaFlowEfficiencyConfigGroup;

public class LimaPoC3Configurator {
	public static void configure(Controler controler) {
		Config config = controler.getConfig();
		LimaAvConfigGroup avConfig = (LimaAvConfigGroup) config.getModules().get(LimaAvConfigGroup.GROUP_NAME);

		if (avConfig != null && avConfig.isUseAv()) {
			LimaPoC3Checker.checkAv(config);

			controler.addOverridingModule(new AbstractModule() {
				@Override
				public void install() {
					AVConfigGroup avConfigGroup = new AVConfigGroup();
					avConfigGroup.setParallelRouters(getConfig().global().getNumberOfThreads());
					bind(AVConfigGroup.class).toInstance(avConfigGroup);
				}
			});

			controler.addOverridingModule(new DvrpTravelTimeModule());
			controler.addOverridingModule(new AVModule());
			controler.addOverridingModule(new IntermodalAVModule());
			controler.addOverridingModule(new LimaAvModule());

			controler.addOverridingModule(new LimaPoC3QSimModule(true, true));
		} else {
			LimaPoC3Checker.checkNoAv(config);
		}

		LimaFlowEfficiencyConfigGroup flowEfficiencyConfig = (LimaFlowEfficiencyConfigGroup) config.getModules()
				.get(LimaFlowEfficiencyConfigGroup.GROUP_NAME);

		if (flowEfficiencyConfig != null && flowEfficiencyConfig.getUseFlowEfficiency()) {
			LimaPoC3Checker.checkFlowEfficiency(config);

			controler.addOverridingModule(new LimaFlowEfficiencyModule());
		}

		LimaModeChoiceConfigGroup modeChoiceConfig = (LimaModeChoiceConfigGroup) config.getModules()
				.get(LimaModeChoiceConfigGroup.GROUP_NAME);

		if (modeChoiceConfig != null && modeChoiceConfig.getUseDiscreteModeChoice()) {
			if (modeChoiceConfig.getUseImportanceSampling()) {
				DiscreteModeChoiceConfigurator.configureAsImportanceSampler(config);

				DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
						.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);
				dmcConfig.setTourEstimator(EstimatorModule.CUMULATIVE);
				dmcConfig.setTripEstimator(LimaDiscreteModeChoiceModule.ESTIMATOR_NAME);
			} else {
				DiscreteModeChoiceConfigurator.configureAsSubtourModeChoiceReplacement(config);
			}

			if (modeChoiceConfig.getOperatingAreaLinkAttribute() != null) {
				DiscreteModeChoiceConfigGroup dmcConfig = (DiscreteModeChoiceConfigGroup) config.getModules()
						.get(DiscreteModeChoiceConfigGroup.GROUP_NAME);
				LinkAttributeConstraintConfigGroup linkAttributeConfig = dmcConfig
						.getLinkAttributeConstraintConfigGroup();
				linkAttributeConfig.setAttributeName(modeChoiceConfig.getOperatingAreaLinkAttribute());
				linkAttributeConfig.setAttributeValue("true");
				linkAttributeConfig.setConstrainedModes(Arrays.asList(AVDirectRoutingModule.AV_DIRECT_MODE));
			}
			
			controler.addOverridingModule(new LimaDiscreteModeChoiceModule());
			controler.addOverridingModule(new DiscreteModeChoiceModule());
		}
	}

}
