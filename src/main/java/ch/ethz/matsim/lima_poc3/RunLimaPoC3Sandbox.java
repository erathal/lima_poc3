package ch.ethz.matsim.lima_poc3;

import java.util.HashSet;

import org.matsim.api.core.v01.Scenario;
import org.matsim.api.core.v01.network.Link;
import org.matsim.contrib.dvrp.run.DvrpConfigGroup;
import org.matsim.core.config.Config;
import org.matsim.core.config.ConfigWriter;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ActivityParams;
import org.matsim.core.config.groups.PlanCalcScoreConfigGroup.ModeParams;
import org.matsim.core.config.groups.SubtourModeChoiceConfigGroup;
import org.matsim.core.controler.Controler;
import org.matsim.core.controler.OutputDirectoryHierarchy.OverwriteFileSetting;

import ch.ethz.matsim.lima_poc3.av.config.LimaAvConfigGroup;
import ch.ethz.matsim.lima_poc3.av.config.ScoringParameterSet;
import ch.ethz.matsim.lima_poc3.dmc.LimaModeChoiceConfigGroup;
import ch.ethz.matsim.lima_poc3.flow_efficiency.config.LimaFlowEfficiencyConfigGroup;
import ch.ethz.matsim.lima_poc3.misc.sandbox.SandboxFactory;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup;
import ch.sbb.matsim.config.SwissRailRaptorConfigGroup.IntermodalAccessEgressParameterSet;
import ch.sbb.matsim.routing.pt.raptor.SwissRailRaptorModule;

public class RunLimaPoC3Sandbox {
	static public void main(String[] args) {
		// Create sandbox scenario
		SandboxFactory factory = new SandboxFactory();

		Scenario scenario = factory.create();
		Config config = scenario.getConfig();

		// Some initial configuration
		config.controler().setOutputDirectory("output_poc3");
		config.controler().setLastIteration(200);
		config.controler().setOverwriteFileSetting(OverwriteFileSetting.deleteDirectoryIfExists);

		// Prepare all necessary settings for LIMA to use AVs
		SwissRailRaptorConfigGroup srrConfig = new SwissRailRaptorConfigGroup();
		config.addModule(srrConfig);

		// Set up intermodal SwissRailraptor with av_feeder
		srrConfig.setUseIntermodalAccessEgress(true);

		IntermodalAccessEgressParameterSet intermodalAccessEgressParameterSet = new IntermodalAccessEgressParameterSet();
		intermodalAccessEgressParameterSet.setMode("av_feeder");
		intermodalAccessEgressParameterSet.setRadius(5000.0);
		srrConfig.addIntermodalAccessEgress(intermodalAccessEgressParameterSet);

		// Set up scoring
		PlanCalcScoreConfigGroup scoringConfig = config.planCalcScore();

		ModeParams feederModeParams = new ModeParams("av_feeder");
		scoringConfig.addModeParams(feederModeParams);

		ModeParams directModeParams = new ModeParams("av_direct");
		scoringConfig.addModeParams(directModeParams);

		ActivityParams interactionParams = new ActivityParams("av interaction");
		interactionParams.setScoringThisActivityAtAll(false);
		scoringConfig.addActivityParams(interactionParams);

		// Set up SubtourModeChoice
		SubtourModeChoiceConfigGroup smcConfig = config.subtourModeChoice();
		smcConfig.setModes(new String[] { "car", "walk", "pt", "av_direct" });

		// Add LIMA AV config group
		LimaAvConfigGroup limaAvConfigGroup = new LimaAvConfigGroup();
		config.addModule(limaAvConfigGroup);

		// Set up scoring parameters for "null" subpopulation
		ScoringParameterSet waitingTimeScoring = new ScoringParameterSet();
		limaAvConfigGroup.addScoringParameterForSubpopulation(waitingTimeScoring);

		limaAvConfigGroup.getConstantSpatialCapacityConfig().setPickupDropoffCapacity(1000);
		limaAvConfigGroup.getConstantSpatialCapacityConfig().setParkingCapacity(10);

		// Add DVRP to config
		DvrpConfigGroup dvrpConfigGroup = new DvrpConfigGroup();
		config.addModule(dvrpConfigGroup);
		dvrpConfigGroup.setNetworkMode("av");

		// For testing: Make all links accessible by AVs
		for (Link link : scenario.getNetwork().getLinks().values()) {
			HashSet<String> allowedModes = new HashSet<>(link.getAllowedModes());
			allowedModes.add("av");
			link.setAllowedModes(allowedModes);
			link.getAttributes().putAttribute(limaAvConfigGroup.getInteractionLinkAttribute(), true);
		}

		// Prepare flow efficiency
		LimaFlowEfficiencyConfigGroup flowEfficiencyConfig = new LimaFlowEfficiencyConfigGroup();
		config.addModule(flowEfficiencyConfig);

		// Prepare discrete mode choice
		LimaModeChoiceConfigGroup modeChoiceConfig = new LimaModeChoiceConfigGroup();
		config.addModule(modeChoiceConfig);

		// Set up controller
		Controler controler = new Controler(scenario);
		controler.addOverridingModule(new SwissRailRaptorModule());
		LimaPoC3Configurator.configure(controler);
		
		new ConfigWriter(config).write("config.xml");

		controler.run();
	}
}
