package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.api.core.v01.network.Link;

public class ConstantFlowEfficiency implements FlowEfficiency {
	private final double flowEfficiency;

	public ConstantFlowEfficiency(double flowEfficiency) {
		this.flowEfficiency = flowEfficiency;
	}

	@Override
	public double getFlowEfficiency(Link link) {
		return flowEfficiency;
	}
}
