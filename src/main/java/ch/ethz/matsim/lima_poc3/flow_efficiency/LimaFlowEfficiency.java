package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.api.core.v01.network.Link;

public class LimaFlowEfficiency implements FlowEfficiency {
	private final double defaultFlowEfficiency;
	private final String linkAttribute;

	public LimaFlowEfficiency(String linkAttribute, double defaultFlowEfficiency) {
		this.linkAttribute = linkAttribute;
		this.defaultFlowEfficiency = defaultFlowEfficiency;
	}

	@Override
	public double getFlowEfficiency(Link link) {
		Double flowEfficiency = (Double) link.getAttributes().getAttribute(linkAttribute);

		if (flowEfficiency == null) {
			return defaultFlowEfficiency;
		} else {
			return flowEfficiency;
		}
	}
}
