package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.api.core.v01.Id;
import org.matsim.api.core.v01.network.Link;
import org.matsim.vehicles.EngineInformation;
import org.matsim.vehicles.VehicleCapacity;
import org.matsim.vehicles.VehicleType;
import org.matsim.vehicles.VehicleUtils;

public class LimaVehicleType implements VehicleType {
	private final VehicleType vehicleType = VehicleUtils.getDefaultVehicleType();
	private final FlowEfficiency flowEffciencyCalculator;
	private final Id<VehicleType> id;

	public LimaVehicleType(FlowEfficiency flowEffciencyCalculator, Id<VehicleType> id) {
		this.flowEffciencyCalculator = flowEffciencyCalculator;
		this.id = id;
	}

	public LimaVehicleType(FlowEfficiency flowEffciencyCalculator) {
		this(flowEffciencyCalculator, null);
	}
	
	@Override
	public Id<VehicleType> getId() {
		if (id == null) {
			return vehicleType.getId();
		} else {
			return id;
		}
	}
	
	public void setDescription(String desc) {
		vehicleType.setDescription(desc);
	}

	public void setLength(double length) {
		vehicleType.setLength(length);
	}

	public void setWidth(double width) {
		vehicleType.setWidth(width);
	}

	public void setMaximumVelocity(double meterPerSecond) {
		vehicleType.setMaximumVelocity(meterPerSecond);
	}

	public void setEngineInformation(EngineInformation currentEngineInfo) {
		vehicleType.setEngineInformation(currentEngineInfo);
	}

	public void setCapacity(VehicleCapacity capacity) {
		vehicleType.setCapacity(capacity);
	}

	public double getWidth() {
		return vehicleType.getWidth();
	}

	public double getMaximumVelocity() {
		return vehicleType.getMaximumVelocity();
	}

	public double getLength() {
		return vehicleType.getLength();
	}

	public EngineInformation getEngineInformation() {
		return vehicleType.getEngineInformation();
	}

	public String getDescription() {
		return vehicleType.getDescription();
	}

	public VehicleCapacity getCapacity() {
		return vehicleType.getCapacity();
	}

	public double getAccessTime() {
		return vehicleType.getAccessTime();
	}

	public void setAccessTime(double seconds) {
		vehicleType.setAccessTime(seconds);
	}

	public double getEgressTime() {
		return vehicleType.getEgressTime();
	}

	public void setEgressTime(double seconds) {
		vehicleType.setEgressTime(seconds);
	}

	public DoorOperationMode getDoorOperationMode() {
		return vehicleType.getDoorOperationMode();
	}

	public void setDoorOperationMode(DoorOperationMode mode) {
		vehicleType.setDoorOperationMode(mode);
	}

	public double getPcuEquivalents() {
		return vehicleType.getPcuEquivalents();
	}

	public void setPcuEquivalents(double pcuEquivalents) {
		vehicleType.setPcuEquivalents(pcuEquivalents);
	}

	public double getFlowEfficiencyFactor(Link link) {
		return flowEffciencyCalculator.getFlowEfficiency(link);
	}

	public void setFlowEfficiencyFactor(double flowEfficiencyFactor) {
		vehicleType.setFlowEfficiencyFactor(flowEfficiencyFactor);
	}
}
