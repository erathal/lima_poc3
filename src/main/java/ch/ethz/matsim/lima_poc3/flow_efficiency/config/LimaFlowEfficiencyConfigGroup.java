package ch.ethz.matsim.lima_poc3.flow_efficiency.config;

import org.matsim.core.config.ReflectiveConfigGroup;

public class LimaFlowEfficiencyConfigGroup extends ReflectiveConfigGroup {
	public final static String GROUP_NAME = "lima_flow_efficiency";

	public final static String USE_FLOW_EFFICIENCY = "useFlowEfficiency";
	public final static String LINK_ATTRIBUTE = "linkAttribute";
	public final static String CONSTANT_FLOW_EFFICIENCY = "constantFlowEfficiency";

	private boolean useFlowEfficiency = true;
	private String linkAttribute = null;
	private double constantFlowEfficiency = 1.5;

	public LimaFlowEfficiencyConfigGroup() {
		super(GROUP_NAME);
	}

	@StringGetter(USE_FLOW_EFFICIENCY)
	public boolean getUseFlowEfficiency() {
		return useFlowEfficiency;
	}

	@StringSetter(USE_FLOW_EFFICIENCY)
	public void setUseFlowEfficiency(boolean useFlowEfficiency) {
		this.useFlowEfficiency = useFlowEfficiency;
	}

	@StringGetter(LINK_ATTRIBUTE)
	public String getLinkAttribute() {
		return linkAttribute;
	}

	@StringSetter(LINK_ATTRIBUTE)
	public void setLinkAttribute(String linkAttribute) {
		this.linkAttribute = linkAttribute;
	}

	@StringGetter(CONSTANT_FLOW_EFFICIENCY)
	public double getConstantFlowEfficiency() {
		return constantFlowEfficiency;
	}

	@StringSetter(CONSTANT_FLOW_EFFICIENCY)
	public void setConstantFlowEfficiency(double constantFlowEfficiency) {
		this.constantFlowEfficiency = constantFlowEfficiency;
	}
}
