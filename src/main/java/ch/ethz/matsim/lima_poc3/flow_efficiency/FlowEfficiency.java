package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.api.core.v01.network.Link;

public interface FlowEfficiency {
	double getFlowEfficiency(Link link);
}
