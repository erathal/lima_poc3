package ch.ethz.matsim.lima_poc3.flow_efficiency;

import org.matsim.core.controler.AbstractModule;
import org.matsim.vehicles.VehicleType;

import com.google.inject.Key;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import com.google.inject.name.Named;
import com.google.inject.name.Names;

import ch.ethz.matsim.av.framework.AVModule;
import ch.ethz.matsim.lima_poc3.flow_efficiency.config.LimaFlowEfficiencyConfigGroup;

public class LimaFlowEfficiencyModule extends AbstractModule {
	@Override
	public void install() {
		LimaFlowEfficiencyConfigGroup config = (LimaFlowEfficiencyConfigGroup) getConfig().getModules()
				.get(LimaFlowEfficiencyConfigGroup.GROUP_NAME);

		if (config.getUseFlowEfficiency()) {
			String linkAttribute = config.getLinkAttribute();

			if (linkAttribute == null) {
				bind(FlowEfficiency.class).to(ConstantFlowEfficiency.class);
			} else {
				bind(FlowEfficiency.class).to(LimaFlowEfficiency.class);
			}

			bind(Key.get(VehicleType.class, Names.named(AVModule.AV_MODE)))
					.to(Key.get(VehicleType.class, Names.named("lima")));
		}
	}

	@Provides
	@Singleton
	public ConstantFlowEfficiency provideConstantFlowEfficiency(LimaFlowEfficiencyConfigGroup config) {
		return new ConstantFlowEfficiency(config.getConstantFlowEfficiency());
	}

	@Provides
	@Singleton
	public LimaFlowEfficiency provideLimaFlowEfficiency(LimaFlowEfficiencyConfigGroup config) {
		return new LimaFlowEfficiency(config.getLinkAttribute(), config.getConstantFlowEfficiency());
	}

	@Provides
	@Named("lima")
	public VehicleType provideDVRPVehicleType(FlowEfficiency flowEffciencyCalculator) {
		return new LimaVehicleType(flowEffciencyCalculator);
	}
}
